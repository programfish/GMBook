package model.dbbean;

import org.litepal.crud.DataSupport;

/**
 * Created by PENG on 2017/12/12.
 */

public class ReadProgress extends DataSupport {
    private String bookId;
    private String bookName;
    private Integer position;




    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getPosition() {
        return position;
    }
}

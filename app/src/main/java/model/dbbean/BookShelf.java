package model.dbbean;

import org.litepal.crud.DataSupport;

/**
 * Created by Administrator on 2017/12/14.
 */

 public class BookShelf extends DataSupport{
    
     private String book_id;
     private String author;
     private String cover;
     private String title;//书名
     private String lastChapter;
     private String updated;
     private Long readTiem;
    private  Integer chapterCount;

    public void setChapterCount(Integer chapterCount) {
        this.chapterCount = chapterCount;
    }

    public Integer getChapterCount() {
        return chapterCount;
    }

    public void setReadTiem(Long readTiem) {
        this.readTiem = readTiem;
    }

    public Long getReadTiem() {
        return readTiem;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getBook_id() {
        return book_id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getLastChapter() {
        return lastChapter;
    }

    public void setLastChapter(String lastChapter) {
        this.lastChapter = lastChapter;
    }
}

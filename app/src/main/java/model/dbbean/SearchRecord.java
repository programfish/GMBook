package model.dbbean;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * Created by Administrator on 2017/12/29.
 */

public class SearchRecord extends DataSupport{
    private String recordContent;
    private long searchTime;

    public void setSearchTime(long searchTime) {
        this.searchTime = searchTime;
    }

    public long getSearchTime() {
        return searchTime;
    }

    public void setRecord(String record) {
        this.recordContent = record;
    }

    public String getRecord() {
        return recordContent;
    }
}

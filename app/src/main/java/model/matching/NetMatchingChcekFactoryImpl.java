package model.matching;

import org.json.JSONException;
import org.json.JSONObject;

import base.Constant;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.OnDataResult;
import okhttp3.Response;
import util.GsonUtil;
import util.LogUtil;

/**
 * Created by PENG on 2018/4/11.
 */

public class NetMatchingChcekFactoryImpl implements NetMatchingChcekFactory {

    private static final String TAG = "NetMatchingChcekFactoryImpl";

    @Override
    public void execute(Flowable<Response> flowable, final Class aClass, final Integer code, final OnDataResult result) {
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response>() {
                    @Override
                    public void accept(@NonNull Response t) throws Exception {
                        obtainCode(code, t, aClass, result);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        LogUtil.d(TAG, throwable.toString());
                        result.onHttpFail("网络访问错误");
                    }
                });
    }

    private void obtainCode(int checkCode, Response response, Class aClass, OnDataResult result) {
        try {
            String json = response.toString();
            JSONObject jsonObject = new JSONObject(json);
            int requestCode = Integer.parseInt(jsonObject.optString(Constant.HTTP_CEKCK_CODE_FLAG));
            if (requestCode == checkCode) {
                LogUtil.d(TAG, "请求码成功匹配");
                transfiguration(json, aClass, result);
            }else {
                String error = jsonObject.optString(Constant.HTTP_CEKCK_MSG_FLAG);
                LogUtil.d(TAG, "请求码不匹配，错误信息："+error);
                result.onHttpFail(error);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void transfiguration(String json, Class aClass, OnDataResult result) {
        result.onHttpSuccess(GsonUtil.parseJsonWithGson(json,aClass));
    }


}

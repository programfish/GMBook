package model.matching;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.OnDataResult;
import util.LogUtil;

/**
 * Created by PENG on 2018/4/11.
 */

public class NetMatchingDirectFactoryImpl implements NetMatchingDirectFactory {

    private static final String TAG = "NetMatchingDirectFactoryImpl";

    @Override
    public <T> void execute(Flowable<T> flowable, final OnDataResult result) {
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<T>() {
                    @Override
                    public void accept(@NonNull T t) throws Exception {
                        result.onHttpSuccess(t);
                        LogUtil.d(TAG, "网络访问成功");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        LogUtil.d(TAG, throwable.toString());
                        result.onHttpFail("网络访问错误");
                    }
                });

    }
}

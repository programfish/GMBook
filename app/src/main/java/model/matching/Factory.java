package model.matching;

import io.reactivex.Flowable;
import model.OnDataResult;
import okhttp3.Response;

/**
 * Created by PENG on 2018/4/11.
 */

public abstract class Factory {

    public abstract <T> void create(Flowable<T> flowable, OnDataResult result);

    public abstract void create(Flowable<Response> flowable, Class aClass, Integer Code, OnDataResult result);
}

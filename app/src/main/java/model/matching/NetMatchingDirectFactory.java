package model.matching;

import io.reactivex.Flowable;
import model.OnDataResult;

/**
 * Created by PENG on 2018/4/11.
 */

public interface NetMatchingDirectFactory {

     <T> void execute(Flowable<T> flowable, OnDataResult result);
}

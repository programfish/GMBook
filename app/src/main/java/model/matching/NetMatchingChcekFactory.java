package model.matching;

import io.reactivex.Flowable;
import model.OnDataResult;
import okhttp3.Response;

/**
 * Created by PENG on 2018/4/11.
 */

public interface NetMatchingChcekFactory {
    void execute(Flowable<Response> flowable, Class aClass, Integer Code, OnDataResult result);
}

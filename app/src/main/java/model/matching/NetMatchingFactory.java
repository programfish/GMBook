package model.matching;

import io.reactivex.Flowable;
import model.OnDataResult;
import okhttp3.Response;

/**
 * Created by PENG on 2018/4/11.
 */

public class NetMatchingFactory extends Factory {

    private NetMatchingFactory() {
    }

    private static class NetMatchingFactoryHolder {
        private static final NetMatchingFactory instance = new NetMatchingFactory();
    }

    public static NetMatchingFactory getNetMatchingFactory() {
        return NetMatchingFactoryHolder.instance;
    }

    private static class NetMatchingDirectFactoryHolder {
        private static final NetMatchingDirectFactoryImpl instance = new NetMatchingDirectFactoryImpl();
    }

    private static NetMatchingDirectFactoryImpl getNetMatchingDirectFactoryInstance() {
        return NetMatchingDirectFactoryHolder.instance;
    }

    private static class NetMatchingChcekFactoryHolder {
        private static final NetMatchingChcekFactoryImpl instance = new NetMatchingChcekFactoryImpl();
    }

    private static NetMatchingChcekFactoryImpl getNetMatchingChcekFactory() {
        return NetMatchingChcekFactoryHolder.instance;
    }

    @Override
    public <T> void create(Flowable<T> flowable, OnDataResult result) {
        checkOnDataResult(result);
        getNetMatchingDirectFactoryInstance().execute(flowable, result);
    }

    @Override
    public void create(Flowable<Response> flowable, Class aClass, Integer code, OnDataResult result) {
        if (code == null) {
            try {
                throw new Throwable("HTTP 检验码不能为Null");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        checkOnDataResult(result);
        getNetMatchingChcekFactory().execute(flowable, aClass, code, result);
    }

    public void checkOnDataResult(OnDataResult result) {
        if (result == null) {
            try {
                throw new Throwable("onDataResult不能为Null");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }


}

package model;

import org.litepal.crud.DataSupport;

import java.util.List;

import util.LogUtil;

/**
 * Created by PENG on 2017/12/14.
 */

public class BookDB {


    public static <T> void save(T t,String hint){
        check(t);
        if(((DataSupport) t).save()){
            LogUtil.d(hint+"保存成功");
        }else {
            LogUtil.d(hint+"保存失败");
        }
    }

    public static <T> List<T> selectList(Class<T> t){
        return DataSupport.findAll(t);
    }

    public static <T> List<T> selectListOrder(String column, Class<T> t){
        return DataSupport.order(column).find(t);
    }

    public static <T> T selectFirst( Class<T> t, String... conditions){
        return DataSupport.where(conditions).findFirst(t);
    }

    public static <T> void updata(T t, String... conditions){
        check(t);
        LogUtil.d("更新数据受影响的数据"+((DataSupport)t).updateAll(conditions));

    }

    public static <T> void delete(Class<T> tClass,String... conditions){
        LogUtil.d("删除数据受影响的数据"+DataSupport.deleteAll(tClass,conditions));
    }

    public static <T> void deleteAll(Class<T> tClass){
        LogUtil.d("删除数据受影响的数据"+DataSupport.deleteAll(tClass));
    }

    public static <T> Boolean check(T t){
        if(!(t instanceof DataSupport)){
            throw new IllegalArgumentException("进行操作数据库的对象类没有继承DataSupport");
        }
        return true;
    }
}

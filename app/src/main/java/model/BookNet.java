package model;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ui.viewInterface.MainView;

/**
 * Created by PENG on 2017/11/29.
 */

public class BookNet {
    public  static <T> void execute(MainView view, final int type, Flowable<T> flowable, final BaseOnDataResult result){
        execute(view,type,flowable,result,null);
    }

    public  static <T> void execute(final MainView view,final int type, Flowable<T> flowable, final BaseOnDataResult result, final Integer position){
                flowable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<T>() {
                    @Override
                    public void accept(@NonNull T t) throws Exception {
                        if(result!=null){
                            if(position==null){
                                result.onHttpSuccess(view,type,t);
                            }else {
                                result.onHttpSuccess(view,type,t,position);
                            }
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        if(result!=null){
                            if(position==null){
                                result.onHttpFail(view,type,throwable.toString());
                            }else {
                                result.onHttpFail(view,type,throwable.toString(),position);
                            }

                        }
                    }
                });
    }
}

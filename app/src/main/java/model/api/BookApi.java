package model.api;

import java.util.List;

import base.Constant;
import io.reactivex.Flowable;
import model.httpbean.AutoComplete;
import model.httpbean.BookDetail;
import model.httpbean.BookHelp;
import model.httpbean.BookHelpList;
import model.httpbean.BookListDetail;
import model.httpbean.BookListTags;
import model.httpbean.BookLists;
import model.httpbean.BookMixAToc;
import model.httpbean.BookRead;
import model.httpbean.BookReview;
import model.httpbean.BookReviewList;
import model.httpbean.BookSource;
import model.httpbean.BooksByCats;
import model.httpbean.BooksByTag;
import model.httpbean.CategoryList;
import model.httpbean.CategoryListLv2;
import model.httpbean.ChapterRead;
import model.httpbean.CommentList;
import model.httpbean.DiscussionList;
import model.httpbean.Disscussion;
import model.httpbean.HotReview;
import model.httpbean.HotWord;
import model.httpbean.RankingList;
import model.httpbean.Rankings;
import model.httpbean.Recommend;
import model.httpbean.RecommendBookList;
import model.httpbean.SearchDetail;
import model.httpbean.SplashBean;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Query;

public class BookApi {

    public static BookApi instance;

    private BookApiService service;

    public BookApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.API_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient)
                .build();
        service = retrofit.create(BookApiService.class);
    }

    public static BookApi getInstance() {
        return ApiHolder.instance;
    }

    private static class ApiHolder{
        private static final BookApi instance = new BookApi();
    }

    public Flowable<SplashBean> getSplashBean(String url) {
        return service.getSplashBean(url);
    }

    public Flowable<Recommend> getRecommend(String gender) {
        return service.getRecomend(gender);
    }

    public Flowable<HotWord> getHotWord() {
        return service.getHotWord();
    }

    public Flowable<AutoComplete> getAutoComplete(String query) {
        return service.autoComplete(query);
    }

    public Flowable<SearchDetail> getSearchResult(String query) {
        return service.searchBooks(query);
    }

    public Flowable<BooksByTag> searchBooksByAuthor(String author) {
        return service.searchBooksByAuthor(author);
    }

    public Flowable<BookDetail> getBookDetail(String bookId) {
        return service.getBookDetail(bookId);
    }

    public Flowable<HotReview> getHotReview(String book) {
        return service.getHotReview(book);
    }

    public Flowable<RecommendBookList> getRecommendBookList(String bookId, String limit) {
        return service.getRecommendBookList(bookId, limit);
    }

    public Flowable<BooksByTag> getBooksByTag(String tags, String start, String limit) {
        return service.getBooksByTag(tags, start, limit);
    }

    public Flowable<BookMixAToc> getBookMixAToc(String bookId, String view) {
        return service.getBookMixAToc(bookId, view);
    }

    public synchronized Flowable<ChapterRead> getChapterRead(String url) {
        return service.getChapterRead(url);
    }

    public synchronized Flowable<List<BookSource>> getBookSource(String view, String book) {
        return service.getABookSource(view, book);
    }

    public Flowable<RankingList> getRanking() {
        return service.getRanking();
    }

    public Flowable<Rankings> getRanking(String rankingId) {
        return service.getRanking(rankingId);
    }

    public Flowable<BookLists> getBookLists(String duration, String sort, String start, String limit, String tag, String gender) {
        return service.getBookLists(duration, sort, start, limit, tag, gender);
    }

    public Flowable<BookListTags> getBookListTags() {
        return service.getBookListTags();
    }

    public Flowable<BookListDetail> getBookListDetail(String bookListId) {
        return service.getBookListDetail(bookListId);
    }

    public synchronized Flowable<CategoryList> getCategoryList() {
        return service.getCategoryList();
    }

    public Flowable<CategoryListLv2> getCategoryListLv2() {
        return service.getCategoryListLv2();
    }

    public Flowable<BooksByCats> getBooksByCats(String gender, String type, String major, String minor, int start, @Query("limit") int limit) {
        return service.getBooksByCats(gender, type, major, minor, start, limit);
    }

    public Flowable<DiscussionList> getBookDisscussionList(String block, String duration, String sort, String type, String start, String limit, String distillate) {
        return service.getBookDisscussionList(block, duration, sort, type, start, limit, distillate);
    }

    public Flowable<Disscussion> getBookDisscussionDetail(String disscussionId) {
        return service.getBookDisscussionDetail(disscussionId);
    }

    public Flowable<CommentList> getBestComments(String disscussionId) {
        return service.getBestComments(disscussionId);
    }

    public Flowable<CommentList> getBookDisscussionComments(String disscussionId, String start, String limit) {
        return service.getBookDisscussionComments(disscussionId, start, limit);
    }

    public Flowable<BookReviewList> getBookReviewList(String duration, String sort, String type, String start, String limit, String distillate) {
        return service.getBookReviewList(duration, sort, type, start, limit, distillate);
    }

    public Flowable<BookRead> getBookRead(String id){
        return service.getBookRead(id);
    }

    public Flowable<BookReview> getBookReviewDetail(String bookReviewId) {
        return service.getBookReviewDetail(bookReviewId);
    }

    public Flowable<CommentList> getBookReviewComments(String bookReviewId, String start, String limit) {
        return service.getBookReviewComments(bookReviewId, start, limit);
    }

    public Flowable<BookHelpList> getBookHelpList(String duration, String sort, String start, String limit, String distillate) {
        return service.getBookHelpList(duration, sort, start, limit, distillate);
    }

    public Flowable<BookHelp> getBookHelpDetail(String helpId) {
        return service.getBookHelpDetail(helpId);
    }

//    public Flowable<Login> login(String platform_uid, String platform_token, String platform_code) {
//        LoginReq loginReq = new LoginReq();
//        loginReq.platform_code = platform_code;
//        loginReq.platform_token = platform_token;
//        loginReq.platform_uid = platform_uid;
//        return service.login(loginReq);
//    }

    public Flowable<DiscussionList> getBookDetailDisscussionList(String book, String sort, String type, String start, String limit) {
        return service.getBookDetailDisscussionList(book, sort, type, start, limit);
    }

    public Flowable<HotReview> getBookDetailReviewList(String book, String sort, String start, String limit) {
        return service.getBookDetailReviewList(book, sort, start, limit);
    }

    public Flowable<DiscussionList> getGirlBookDisscussionList(String block, String duration, String sort, String type, String start, String limit, String distillate) {
        return service.getBookDisscussionList(block, duration, sort, type, start, limit, distillate);
    }

}
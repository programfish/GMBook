package model;

import ui.viewInterface.MainView;

/**
 * Created by PENG on 2017/11/29.
 */

public interface BaseOnDataResult {
    void onHttpSuccess(MainView view,int type, Object object);
    void onHttpFail(MainView view,int type,String error);
    void onHttpSuccess(MainView view,int type,Object object,Integer position);
    void onHttpFail(MainView view,int type,String error,Integer position);
}

package model.httpbean;

import java.util.List;

/**
 * Created by PENG on 2018/4/9.
 */

public class SplashBean {

    /**
     * ads : [{"monitorReplaceDeviceId":"","id":"121294","ratio":"33.33","show_num":3,"sub_title":"","video_url":"","main_title":"云音乐推送广告_7967","res_url":["http://iadmat.nosdn.127.net/ad.bid.material_8106cff1d6934a1bbc4679362aaf8e2e?imageView&thumbnail=1080x1920&quality=100","","",""],"action_params":{"link_url":"orpheus://openurl?url=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN566410.3052434%2FB20944471.218537983%3Bdc_trk_aid%3D417126098%3Bdc_trk_cid%3D100025535%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D"},"thirdplat":0,"location":"1","action":"1","ad_type":0,"adv_id":"108491","monitor":"","ad_loc":6,"pass_info":{"isfixed":"0","clip":"0"},"monitorShowUrl":"","category":"STARTUP","service":8,"is_dsp_backup":"","expired_time":"1523375999000","flight_id":"942","content":"","ext_info":{},"is_sens":0,"style":"2","show_time":"3.0","loop_top_priority":0,"monitorClickUrl":""},{"monitorReplaceDeviceId":"","id":"121296","ratio":"33.33","show_num":3,"sub_title":"","video_url":"","main_title":"云音乐推送广告_7970","res_url":["http://iadmat.nosdn.127.net/ad.bid.material_aa33513a76e44a2ba96c4f26ecf54d37?imageView&thumbnail=1080x1920&quality=100","","",""],"action_params":{"link_url":"orpheus://openurl?url=https%3A%2F%2Fwww.adidas.com.cn%2Foriginals_cityrun%3Fcm_mmc%3DAdiDisplay_Targeting-_-Netease_music-_-Mobile-_-opening-_-dv%3ABrand-_-cn%3Acity_run-_-pc%3Aoriginals%26cm_mmc1%3DCN%26utm_source%3DNetease_music%26utm_medium%3DDisplay%26utm_campaign%3Dcity_run%26utm_content%3Dopening"},"thirdplat":0,"location":"1","action":"1","ad_type":0,"adv_id":"108493","monitor":"","ad_loc":6,"pass_info":{"isfixed":"0","clip":"0"},"monitorShowUrl":"","category":"STARTUP","service":8,"is_dsp_backup":"","expired_time":"1523375999000","flight_id":"942","content":"","ext_info":{},"is_sens":0,"style":"2","show_time":"3.0","loop_top_priority":0,"monitorClickUrl":""},{"monitorReplaceDeviceId":"","id":"121209","ratio":"33.33","show_num":3,"sub_title":"","category":"STARTUP","main_title":"云音乐推送广告_7950","res_url":["http://iadmat.nosdn.127.net/ad.bid.material_3ad4f1ccb5f145e0919f3257bcfb60fb?imageView&thumbnail=1080x1920&quality=100","","",""],"action_params":{"link_url":"orpheus://openurl?url=https%3A%2F%2Fad.api.biligame.net%2Fapi%2Fv1%2Fad%2Fplan%2Fdetail%3Fid%3D1611"},"thirdplat":0,"location":"1","action":"1","ad_type":0,"adv_id":"108421","monitor":"","ad_loc":6,"pass_info":{"isfixed":"0","clip":"0"},"monitorShowUrl":"","video_url":"","service":8,"is_dsp_backup":"","expired_time":"1523375999000","flight_id":"942","content":"","ext_info":{},"is_sens":0,"style":"2","show_time":"3.0","loop_top_priority":0,"monitorClickUrl":""}]
     * result : 0
     * next_req : 600
     * error :
     */

    private int result;
    private int next_req;
    private String error;
    private List<AdsBean> ads;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getNext_req() {
        return next_req;
    }

    public void setNext_req(int next_req) {
        this.next_req = next_req;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<AdsBean> getAds() {
        return ads;
    }

    public void setAds(List<AdsBean> ads) {
        this.ads = ads;
    }

    public static class AdsBean {
        /**
         * monitorReplaceDeviceId :
         * id : 121294
         * ratio : 33.33
         * show_num : 3
         * sub_title :
         * video_url :
         * main_title : 云音乐推送广告_7967
         * res_url : ["http://iadmat.nosdn.127.net/ad.bid.material_8106cff1d6934a1bbc4679362aaf8e2e?imageView&thumbnail=1080x1920&quality=100","","",""]
         * action_params : {"link_url":"orpheus://openurl?url=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN566410.3052434%2FB20944471.218537983%3Bdc_trk_aid%3D417126098%3Bdc_trk_cid%3D100025535%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D"}
         * thirdplat : 0
         * location : 1
         * action : 1
         * ad_type : 0
         * adv_id : 108491
         * monitor :
         * ad_loc : 6
         * pass_info : {"isfixed":"0","clip":"0"}
         * monitorShowUrl :
         * category : STARTUP
         * service : 8
         * is_dsp_backup :
         * expired_time : 1523375999000
         * flight_id : 942
         * content :
         * ext_info : {}
         * is_sens : 0
         * style : 2
         * show_time : 3.0
         * loop_top_priority : 0
         * monitorClickUrl :
         */

        private String monitorReplaceDeviceId;
        private String id;
        private String ratio;
        private int show_num;
        private String sub_title;
        private String video_url;
        private String main_title;
        private ActionParamsBean action_params;
        private int thirdplat;
        private String location;
        private String action;
        private int ad_type;
        private String adv_id;
        private String monitor;
        private int ad_loc;
        private PassInfoBean pass_info;
        private String monitorShowUrl;
        private String category;
        private int service;
        private String is_dsp_backup;
        private String expired_time;
        private String flight_id;
        private String content;
        private ExtInfoBean ext_info;
        private int is_sens;
        private String style;
        private String show_time;
        private int loop_top_priority;
        private String monitorClickUrl;
        private List<String> res_url;

        public String getMonitorReplaceDeviceId() {
            return monitorReplaceDeviceId;
        }

        public void setMonitorReplaceDeviceId(String monitorReplaceDeviceId) {
            this.monitorReplaceDeviceId = monitorReplaceDeviceId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRatio() {
            return ratio;
        }

        public void setRatio(String ratio) {
            this.ratio = ratio;
        }

        public int getShow_num() {
            return show_num;
        }

        public void setShow_num(int show_num) {
            this.show_num = show_num;
        }

        public String getSub_title() {
            return sub_title;
        }

        public void setSub_title(String sub_title) {
            this.sub_title = sub_title;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getMain_title() {
            return main_title;
        }

        public void setMain_title(String main_title) {
            this.main_title = main_title;
        }

        public ActionParamsBean getAction_params() {
            return action_params;
        }

        public void setAction_params(ActionParamsBean action_params) {
            this.action_params = action_params;
        }

        public int getThirdplat() {
            return thirdplat;
        }

        public void setThirdplat(int thirdplat) {
            this.thirdplat = thirdplat;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public int getAd_type() {
            return ad_type;
        }

        public void setAd_type(int ad_type) {
            this.ad_type = ad_type;
        }

        public String getAdv_id() {
            return adv_id;
        }

        public void setAdv_id(String adv_id) {
            this.adv_id = adv_id;
        }

        public String getMonitor() {
            return monitor;
        }

        public void setMonitor(String monitor) {
            this.monitor = monitor;
        }

        public int getAd_loc() {
            return ad_loc;
        }

        public void setAd_loc(int ad_loc) {
            this.ad_loc = ad_loc;
        }

        public PassInfoBean getPass_info() {
            return pass_info;
        }

        public void setPass_info(PassInfoBean pass_info) {
            this.pass_info = pass_info;
        }

        public String getMonitorShowUrl() {
            return monitorShowUrl;
        }

        public void setMonitorShowUrl(String monitorShowUrl) {
            this.monitorShowUrl = monitorShowUrl;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public int getService() {
            return service;
        }

        public void setService(int service) {
            this.service = service;
        }

        public String getIs_dsp_backup() {
            return is_dsp_backup;
        }

        public void setIs_dsp_backup(String is_dsp_backup) {
            this.is_dsp_backup = is_dsp_backup;
        }

        public String getExpired_time() {
            return expired_time;
        }

        public void setExpired_time(String expired_time) {
            this.expired_time = expired_time;
        }

        public String getFlight_id() {
            return flight_id;
        }

        public void setFlight_id(String flight_id) {
            this.flight_id = flight_id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public ExtInfoBean getExt_info() {
            return ext_info;
        }

        public void setExt_info(ExtInfoBean ext_info) {
            this.ext_info = ext_info;
        }

        public int getIs_sens() {
            return is_sens;
        }

        public void setIs_sens(int is_sens) {
            this.is_sens = is_sens;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }

        public int getLoop_top_priority() {
            return loop_top_priority;
        }

        public void setLoop_top_priority(int loop_top_priority) {
            this.loop_top_priority = loop_top_priority;
        }

        public String getMonitorClickUrl() {
            return monitorClickUrl;
        }

        public void setMonitorClickUrl(String monitorClickUrl) {
            this.monitorClickUrl = monitorClickUrl;
        }

        public List<String> getRes_url() {
            return res_url;
        }

        public void setRes_url(List<String> res_url) {
            this.res_url = res_url;
        }

        public static class ActionParamsBean {
            /**
             * link_url : orpheus://openurl?url=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN566410.3052434%2FB20944471.218537983%3Bdc_trk_aid%3D417126098%3Bdc_trk_cid%3D100025535%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D
             */

            private String link_url;

            public String getLink_url() {
                return link_url;
            }

            public void setLink_url(String link_url) {
                this.link_url = link_url;
            }
        }

        public static class PassInfoBean {
        }

        public static class ExtInfoBean {
        }
    }
}

package model.httpbean;

import java.util.List;

/**
 * Created by Administrator on 2017/12/26.
 */

public class ClassificationBean {
    public List<CategoryList.MaleBean> data;
    public String title;

    public void setData(List<CategoryList.MaleBean> data) {
        this.data = data;
    }

    public List<CategoryList.MaleBean> getData() {
        return data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

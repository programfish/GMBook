package model.httpbean;

import java.util.List;

/**
 * Created by PENG on 2017/12/2.
 */

public class BookChapterBean {
    public ChapterRead.Chapter chapter;
    public int position;
    public String title;
    private List<String> stringList;
    private String bookName;
    private int color;

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookName() {
        return bookName;
    }

    public List<String> getStringList() {
        return stringList;
    }


    public ChapterRead.Chapter getChapter() {
        return chapter;
    }

    public void setChapter(ChapterRead.Chapter chapter) {
        this.chapter = chapter;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package presenter;

import android.graphics.Bitmap;

import java.io.UnsupportedEncodingException;

import base.Constant;
import base.Global;
import model.OnDataResult;
import model.api.BookApi;
import model.httpbean.SplashBean;
import model.matching.NetMatchingFactory;
import ui.viewInterface.SplashInterface;
import util.LogUtil;

/**
 * Created by PENG on 2018/4/9.
 */

public class SplashPresenter extends BasePresenter<SplashInterface> implements OnDataResult<SplashBean> {

    private Long requestTime;

    private static final String TAG = "SplashPresenter";

    public SplashPresenter(SplashInterface view) {
        super(view);
    }

    public void request(long requestTime) {
        this.requestTime = requestTime;
        NetMatchingFactory.getNetMatchingFactory()
                  .create(BookApi.getInstance()
                          .getSplashBean(Constant.SPLASH_URL), this);
        LogUtil.d(TAG, "开始请求网络");
    }

    @Override
    public void onHttpSuccess(SplashBean data) {
        if (!isViewAttached()) {
            return;
        }
        long currentTime = System.currentTimeMillis();
        if ((currentTime - requestTime) <= Constant.SPLASH_WAIT_TIME) {
            SplashBean.AdsBean adsBean = data.getAds().get(Global.randomNumber(data.getAds().size()));
            String linkUrl = checkLink(adsBean.getAction_params().getLink_url());
            String url = adsBean.getRes_url().get(0);
            getReadyBitmap(url,linkUrl);

        } else {
            LogUtil.d(TAG, "网络超时 不执行加载广告图片");
        }
    }


    private void getReadyBitmap(String url, final String linkUrl) {
        obtainBitmap(url, new OnDataResult<Bitmap>() {

            @Override
            public void onHttpSuccess(Bitmap data) {
                if (!isViewAttached()) {
                    return;
                }
                mView.get().showImage(data, linkUrl);
            }

            @Override
            public void onHttpFail(String error) {

            }
        });
    }

    /**
     * 进行广告页检测是否有广告链接
     *
     * @param linkUrl
     * @return
     */
    private String checkLink(String linkUrl) {
        String checkStr = "url=";

        int index = linkUrl.indexOf(checkStr);

        if (index != -1) {
            index += checkStr.length();
            try {
                linkUrl = linkUrl.substring(index, linkUrl.length() - 1);
                String output = java.net.URLDecoder.decode(linkUrl, "UTF-8");
                LogUtil.d(TAG, output);
                return output;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    @Override
    public void onHttpFail(String error) {
        if (!isViewAttached()) {
            return;
        }
        mView.get().requestFail();
    }
}

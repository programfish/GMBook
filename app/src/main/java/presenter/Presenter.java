package presenter;

/**
 * Created by PENG on 2017/12/16.
 */

public class Presenter {

    private static MainPresenter instance;

    public static MainPresenter getInstance(){
        if (instance == null)
            instance = new MainPresenter();
        return instance;
    }
}

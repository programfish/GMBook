package presenter;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.peng.book.R;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

import base.GlideApp;
import base.Global;
import model.OnDataResult;
import util.LogUtil;

/**
 * Created by PENG on 2018/4/9.
 */

public abstract class BasePresenter<T> {
    protected Reference<T> mView;
    private static final String TAG  = "BasePresenter";

    protected void loadPicture(ImageView imageView,String url){
        GlideApp.with(Global.mContext)
                .asBitmap()
                .load(url)
                .error(R.drawable.vector_drawable_image_fail)
                .load(R.drawable.vector_drawable_image_load)
                .into(imageView);
    }

    protected void obtainBitmap(String url, final OnDataResult onDataResult){
        GlideApp.with(Global.mContext)
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                         onDataResult.onHttpSuccess(resource);
                    }
                });
    }

    public BasePresenter(T view) {
        mView = new WeakReference<T>(view);
    }

    public Reference<T> getmView() {
        return mView;
    }

    public boolean isViewAttached() {
        boolean a = mView != null && mView.get() != null;
        LogUtil.d(TAG, "isViewAttached--"+a);
        return a;
    }

    public void detachView() {
        if (mView != null) {
            mView.clear();
            mView = null;
        }
    }
}

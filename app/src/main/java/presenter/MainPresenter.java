package presenter;

import android.graphics.Paint;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.Constant;
import base.GlideApp;
import base.Global;
import model.BaseOnDataResult;
import model.BookDB;
import model.BookNet;
import model.api.BookApi;
import model.dbbean.ReadProgress;
import model.httpbean.BookChapterBean;
import model.httpbean.ChapterRead;
import model.httpbean.Rankings;
import ui.viewInterface.MainView;
import util.LogUtil;

import static base.Constant.chaptersList;
import static ui.activity.BookReadActivity.aLineWidth;
import static ui.activity.BookReadActivity.firstLineNumber;
import static ui.activity.BookReadActivity.otherLineNumber;
import static util.LogUtil.d;

/**
 * Created by PENG on 2017/11/29.
 */

public class MainPresenter implements BaseOnDataResult{

    public MainPresenter() {
    }

    public void onRecommend(MainView view){
        BookNet.execute(view,Constant.HTTP_TYPE_RECOMMEND,BookApi.getInstance().getRecommend("男"),this);

    }

    public void onBookDetail(MainView view,String id){
        BookNet.execute(view,Constant.HTTP_TYPE_BOOK_DETAILED, BookApi.getInstance().getBookDetail(id),this);
    }

    public void onCatalog(MainView view,String id){
        BookNet.execute(view,Constant.HTTP_TYPE_BOOK_READ,BookApi.getInstance().getBookRead(id),this);
    }

    public void onChapter(MainView view,String link, int position){
        BookNet.execute(view,Constant.HTTP_TYPE_CHAPTER, BookApi.getInstance().getChapterRead(link),this,position);
    }

    public void onClassification(MainView view){
        BookNet.execute(view,Constant.HTTP_TYPE_CLASSIFICATION,BookApi.getInstance().getCategoryList(),this);
    }

    public void onRankingList(MainView view){
        BookNet.execute(view,Constant.HTTP_TYPE_RANKINGLIST,BookApi.getInstance().getRanking(),this);
    }

    public void onRankings(MainView view,String rankingId,int position){
        BookNet.execute(view,Constant.HTTP_TYPE_RANKINGS,BookApi.getInstance().getRanking(rankingId),this,position);
    }

    public void onAutoComplete(MainView view,String query){
        BookNet.execute(view,Constant.HTTP_TYPE_AUTOCOMPLETE,BookApi.getInstance().getAutoComplete(query),this);
    }

    public void onHotWords(MainView view){
        BookNet.execute(view,Constant.HTTP_TYPE_HOTWORDS,BookApi.getInstance().getHotWord(),this);
    }

    public void onSearchBooks(MainView view,String bookName){
        BookNet.execute(view,Constant.HTTP_TYPE_SEARCHBOOKS,BookApi.getInstance().getSearchResult(bookName),this);
    }

    public void onConditionalBooks(MainView view,String gender,String type,String major,String minor,int start,int limit){
        BookNet.execute(view,Constant.HTTP_TYPE_CONDITIONALSBOOKS,
                BookApi.getInstance().getBooksByCats(gender,type,major,minor,start,limit),this);
    }

    public void onCategoryListLv2(MainView view){
        BookNet.execute(view,Constant.HTTP_TYPE_CATEGORYLISTLV2,BookApi.getInstance().getCategoryListLv2(),this);
    }

    public void loadImage(String url, ImageView imageView){
        GlideApp.with(Global.mContext)
                .asBitmap()
                .placeholder(R.drawable.vector_drawable_image_load)
                .error(R.drawable.vector_drawable_image_fail)
                .load(Constant.IMG_BASE_URL+url)
                .into(imageView);
    }

    @Override
    public void onHttpSuccess(MainView view,int type, Object object, Integer position) {
            switch (type){
                case Constant.HTTP_TYPE_CHAPTER:
                    ChapterRead chapterRead= (ChapterRead) object;
                    BookChapterBean bean=new BookChapterBean();
                    bean.chapter=chapterRead.chapter;
                    bean.chapter.body="\u3000\u3000"+bean.chapter.body+"\n\n\u3000\u3000本章完";
                    bean.chapter.body=bean.chapter.body.replaceAll("\n","\n\u3000\u3000");
                    bean.position=position;
                    bean.setColor(R.color.black);
                    bean.title=chaptersList.get(position).title;
                    bean.setBookName(Constant.BOOK_NAME);
                    if(!(firstLineNumber==0||otherLineNumber==0||aLineWidth==0)){
                        screenData(bean);
                    }
                    view.getData(type,bean);
                    break;
                case Constant.HTTP_TYPE_RANKINGS:
                    Rankings rankings= (Rankings) object;
                    rankings.ranking.position=position;
                    view.getData(type,rankings);
                    break;
            }


    }

    @Override
    public void onHttpSuccess(MainView view,int type,Object object) {
            switch (type){
                default:
                    view.getData(type,object);
                    break;
            }
    }


    public <T> void saveData(T t,String hint){
        BookDB.save(t,hint);
    }

    public <T> T selectFirst( Class<T> t,String... conditions){
       return  BookDB.selectFirst(t, conditions);
    }

    public <T> List<T> selectList(Class<T> tClass){
        return BookDB.selectList(tClass);
    }

    public <T> void updata(T t,String... conditions){
        BookDB.updata(t,conditions);
    }

    public <T> void delete(Class<T> t,String... conditions){
        if(conditions.length==0){
            BookDB.deleteAll(t);
        }else {
            BookDB.delete(t,conditions);
        }
    }

    //column asc表示正序排序，desc表示倒序排序
    public <T> List<T> selectListOrder(String column,Class<T> tClass){
       return BookDB.selectListOrder(column,tClass);
    }

    @Override
    public void onHttpFail(MainView view,int type, String error) {
        d("错误类型："+type+"异常情况："+error);
        view.getError(type,error);
    }

    @Override
    public void onHttpFail(MainView view,int type, String error, Integer position) {
        d("错误类型："+type+"异常情况："+error+"位置："+position);
        view.getError(type,position);
    }

    //筛选书架章节状况提示语（未读、已读完、X章未读完）
    public String bookReadProgreen(String bookName,int chapterCount){
        ReadProgress readProgress =
                selectFirst(ReadProgress.class,"bookName=?", bookName);
        String chapterStr;
        if(readProgress==null){
            chapterStr="未读";
        }else {
            int posiont=readProgress.getPosition();
            chapterCount-=1;
            if(posiont==chapterCount){
                chapterStr="已读完";
            }else {
                chapterStr=(chapterCount-posiont)+"章未读完";
            }
        }
        return chapterStr;
    }

    //计算上次更新距离当前时间间隔 注意！！！ 年按365天算，月按30天算
    public String bookUpdateTime(String date){
        date=substringTime(date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date1 = null;
        Date date2=null;
        try {
            date1 = sdf.parse(date);
            date2 = sdf.parse(sdf.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long l = date2.getTime() - date1.getTime();
        int day = (int) (l / (24 * 60 * 60 * 1000));
        int hour = (int) (l / (60 * 60 * 1000) - day * 24);
        int min = (int) ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        int mont=day/30;
        int year= day/365;
        String dateStr="";
        String bDate=sdf.format(date2);
        if(date.equals(bDate)){
            dateStr="刚刚更新";
        }else if(year!=0){
            dateStr=year+"年前更新";
        }else if(mont!=0){
            dateStr=mont+"月前更新";
        }else if(day!=0){
            dateStr=day+"天前更新";
        }else if(hour!=0){
            dateStr=hour+"小时前更新";
        }else if(min!=0){
            dateStr=min+"分钟前更新";
        }
        return dateStr;
    }

    //更新时间字符筛选
    private String substringTime(String date){
        int index=date.indexOf(".")-3;
        date=date.substring(0, index);
        date=date.replace("T", " ");
        return date;
    }

    //筛选章节内容 分页
    public void screenData(BookChapterBean data){
        Paint testPaint= Constant.PAINT;
        if(testPaint==null){
            LogUtil.d("testPaint==null");
        }

        String content=data.getChapter().body;
        if(content==null){
            return;
        }
        List<String> stringList=new ArrayList<>();
        boolean isfirst=true;
        float lineWidth=0;
        int lineNumber=0;
        StringBuffer stringBuffer = null;
        for(int i=0;i<content.length();i++) {
            if (stringBuffer == null) {
                stringBuffer = new StringBuffer();
            }
            String c = String.valueOf(content.charAt(i));

            float strWidth;
            if(c.equals("\n")){
                strWidth=aLineWidth;
            }else {
                strWidth = testPaint.measureText(c);
            }
            lineWidth += strWidth;
            if (lineWidth >=aLineWidth) {
                if(!c.equals("\n")){
                    stringBuffer.append(c);
                }else {
                    stringBuffer.append("\n");
                    lineNumber++;
                }
                lineWidth = 0;
                lineNumber++;
                stringBuffer.append("\n");
            } else {
                stringBuffer.append(c);
            }
            if(i==content.length()-1){
                lineNumber=otherLineNumber;
            }
            if(isfirst){
                if(lineNumber>=firstLineNumber){
                    stringList.add(stringBuffer.toString());
                    stringBuffer=null;
                    isfirst=false;
                    lineNumber=0;
                    lineWidth=0;
                }
            }else {
                if(lineNumber>=otherLineNumber){
                    stringList.add(stringBuffer.toString());
                    stringBuffer=null;
                    lineNumber=0;
                    lineWidth=0;
                }
            }
        }
        data.setStringList(stringList);
        LogUtil.d("Position："+data.getPosition()+"--"+stringList.size()+"页");
        stringList=null;
    }
}

package base;

import android.graphics.Paint;
import android.view.View;

import java.util.List;

import model.httpbean.BookRead;

/**
 * Created by PENG on 2017/11/26.
 */

public class Constant {
    public static final String IMG_BASE_URL = "http://statics.zhuishushenqi.com";

    public static final String API_BASE_URL = "http://api.zhuishushenqi.com";
    public static final String HOST_IP = "";
    public static final String KEY_ID="id";
    public static final String KEY_POSITION="position";
    public static final String KEY_SHOW_MODE="SHOW_MODE";
    public static final String KEY_TEXT_COLOR="TEXT_COLOR";
    public static final String KEY_TEXT_SIZE="TEXT_SIZE";
    public static final String KEY_TYPE="CONDITIONALS_TYPE";
    public static final String KEY_GENDER="CONDITIONALS_GENDER";


    public static final int TYPE_COLOR=0;
    public static final int TYPE_TXETSIZE=1;


    public static final int HTTP_TYPE_RECOMMEND=0;
    public static final int HTTP_TYPE_BOOK_DETAILED=1;
    public static final int HTTP_TYPE_BOOK_READ=2;
    public static final int HTTP_TYPE_CHAPTER=3;
    public static final int HTTP_TYPE_RANKINGLIST=4;
    public static final int HTTP_TYPE_RANKINGS=5;
    public static final int HTTP_TYPE_CLASSIFICATION=6;
    public static final int HTTP_TYPE_HOTWORDS=7;
    public static final int HTTP_TYPE_AUTOCOMPLETE=8;
    public static final int HTTP_TYPE_SEARCHBOOKS=9;
    public static final int HTTP_TYPE_CONDITIONALSBOOKS=10;
    public static final int HTTP_TYPE_CATEGORYLISTLV2=11;

    public static int LONGINTRO_HIGH=0;
    public static String BOOK_NAME;
    public static String BOOK_ID;

    public static List<BookRead.MixToc.Chapters> chaptersList;
    public static View.OnClickListener readOnclickListener;
    public static Paint PAINT;


    public static final String HASHMAP_KEY_IMAGE="KEY_IMAGE";
    public static final String HASHMAP_KEY_TEXT="KEY_TEXT";


    public static final String BOOKS_TYPE_MALE="male";
    public static final String BOOKS_TYPE_FEMALE="female";
    public static final String BOOKS_TYPE_PICTURE="picture";
    public static final String BOOKS_TYPE_PRESS="press";

    public static final String SPLASH_URL = "http://g1.163.com/madr?app=A1D6253F&platform=android&category=STARTUP&location=1&timestamp=1523270649141";

    public static final String BASE_URL = "http://statics.zhuishushenqi.com//";

    public static final int SPLASH_WAIT_TIME = (int) (2 * 1000);

    public static final String LINK_URL_FLAG = "linkUrl";

    public static final String SPLASH_START_WEB_FLAG = "splash_start_web";

    public static final String HTTP_CEKCK_CODE_FLAG = "code";
    public static final String HTTP_CEKCK_MSG_FLAG = "msg";


}

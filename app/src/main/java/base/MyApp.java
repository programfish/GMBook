package base;

import com.squareup.leakcanary.LeakCanary;

import org.litepal.LitePalApplication;

/**
 * 应用程序上下文对象，常作一些初始化操作
 *
 * @author WJQ
 */
public class MyApp extends LitePalApplication {
	
	@Override
	public void onCreate() {
		super.onCreate();
		Global.init(this);
		LeakCanary.install(this);
	}


}

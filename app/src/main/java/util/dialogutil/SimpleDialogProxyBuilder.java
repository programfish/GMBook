package util.dialogutil;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 * Created by PENG on 2018/4/13.
 * 用于存储Dialog数据
 * 防止DialogFragment重建Dialog数据丢失
 */

public class SimpleDialogProxyBuilder {

    private static class ProxyBuilderHolder {
        private static final SimpleDialogProxyBuilder instance = new SimpleDialogProxyBuilder();
    }

    public static SimpleDialogProxyBuilder getInstance() {
        return SimpleDialogProxyBuilder.ProxyBuilderHolder.instance;
    }

    private Reference<SimpleDialogFragment.Builder> builderReference;

    public void setBuilder(SimpleDialogFragment.Builder builder) {
        builderReference = new WeakReference<>(builder);
    }

    public SimpleDialogFragment.Builder getBuilder() {
        if(builderReference != null){
            return builderReference.get();
        }
        return null;
    }

}

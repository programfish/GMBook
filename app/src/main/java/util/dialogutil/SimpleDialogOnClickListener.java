package util.dialogutil;

import android.view.View;

/**
 * Created by PENG on 2018/4/13.
 */

public interface SimpleDialogOnClickListener {
    void onClick(SimpleDialogFragment dialogFragment, View view);
}

package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

/**
 * Created by PENG on 2017/9/17.
 */

public class GsonUtil {
    public static Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();

    public static String GosnToJson(Map map){
        return gson.toJson(map);
    }

    public static <T> T parseJsonWithGson(String jsonData, Class<T> type) {
        Gson gson2=new Gson();
        T result = gson2.fromJson(jsonData, type);
        return result;
    }
}

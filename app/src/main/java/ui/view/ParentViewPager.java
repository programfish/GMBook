package ui.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;

import com.example.peng.book.R;

import base.Global;
import ui.adapter.BookReadChildAdapter;
import ui.adapter.BookReadParentAdapter;

/**
 * Created by PENG on 2018/3/15.
 */

public class ParentViewPager extends ViewPager {


    private int touchSlop;
    private GestureDetector gestureDetector;

    public ParentViewPager(Context context) {
        super(context);
        init();
    }

    private void init() {
        touchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();

    }

    public ParentViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private int downX;
    private int moveX;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercept = false;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = (int) ev.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                moveX = (int) ev.getX();
                intercept = moveTask();
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return intercept;
    }

    /**
     * MOVE事件拦截
     *
     * @return
     */
    private boolean moveTask() {
        boolean intercept = false;
        BookReadParentAdapter adapter = (BookReadParentAdapter) getAdapter();
        int index = getCurrentItem();
        View view;
        if (adapter.getViews() != null) {
            view = adapter.getViews()[index];
            ChildViewPager childViewPager = view.findViewById(R.id.viewpager);
            BookReadChildAdapter childAdapter = (BookReadChildAdapter) childViewPager.getAdapter();
            if (childViewPager != null && childAdapter != null && childAdapter.getViews() != null && childAdapter.getViews().length != 0) {
                if (childViewPager.getCurrentItem() == childViewPager.getAdapter().getCount() - 1) {
                    if (downX - moveX >= touchSlop) {
                        setCurrentItem(getCurrentItem() + 1);
                        if (getCurrentItem() == adapter.getCount() - 1) {
                            Global.showToast("抱歉，没有更多了");
                        }
                        intercept = true;
                    }
                } else if (childViewPager.getCurrentItem() == 0) {
                    if (moveX - downX >= touchSlop) {
                        if (getCurrentItem() != 0) {
                            letfIntercept(adapter.getViews()[index - 1]);
                        } else {
                            Global.showToast("这是第一页");
                        }
                        intercept = true;
                    }
                }
            }
        }
        return intercept;
    }

    /**
     * 左拦截
     *
     * @param view
     */
    private void letfIntercept(View view) {
        if (getCurrentItem() - 1 >= 0) {
            setCurrentItem(getCurrentItem() - 1);
            RelativeLayout beforeView = (RelativeLayout) view;
            ChildViewPager beforeChildViewPager = beforeView.findViewById(R.id.viewpager);
            BookReadChildAdapter beforeAdapter = (BookReadChildAdapter) beforeChildViewPager.getAdapter();
            if (beforeChildViewPager != null && beforeAdapter != null && beforeAdapter.getViews() != null && beforeAdapter.getViews().length != 0) {
                beforeChildViewPager.setCurrentItem(beforeAdapter.getCount() - 1);
            }
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }


}

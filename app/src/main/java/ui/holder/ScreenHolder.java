package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import butterknife.BindView;
import ui.activity.ConditionalsBooksActivity;

/**
 * Created by PENG on 2018/1/1.
 */

public class ScreenHolder extends BaseHolderRV<String> {

    @BindView(R.id.text)
    TextView text;

    public ScreenHolder(Context context, ViewGroup parent, BaseAdapterRV<String> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_screen);
    }

    @Override
    protected void onRefreshView(String bean, int position) {
        text.setText(bean);
    }

    @Override
    protected void onItemClick(View itemView, int position, String bean) {
        ConditionalsBooksActivity activity= (ConditionalsBooksActivity) context;
        switch (activity.index){
            case ConditionalsBooksActivity.MAJOR:
                activity.major=bean;
                activity.minor="";
                activity.adapter.listData.set(ConditionalsBooksActivity.MAJOR,bean);
                activity.adapter.listData.set(ConditionalsBooksActivity.MINOR,"全部");
                break;
            case ConditionalsBooksActivity.MINOR:
                String minor;
                if(position==0){
                    minor=null;
                } else {
                    minor=bean;
                }
                activity.minor=minor;
                activity.adapter.listData.set(ConditionalsBooksActivity.MINOR,bean);
                break;
            case ConditionalsBooksActivity.TYPE:
                activity.adapter.listData.set(ConditionalsBooksActivity.TYPE,bean);
                activity.type=activity.typeList.get(position);
                break;
        }
        activity.adapter.notifyDataSetChanged();
        activity.window.dismiss();
        activity.booksAdapter.listData.clear();
        activity.booksAdapter.notifyDataSetChanged();
        activity.xrecycler.refresh();
        activity.xrecycler.scrollToPosition(0);
    }
}

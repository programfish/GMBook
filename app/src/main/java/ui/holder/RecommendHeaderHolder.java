package ui.holder;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import butterknife.BindView;

/**
 * Created by PENG on 2017/11/29.
 */

public class RecommendHeaderHolder extends BaseHolderRV<String> {


    @BindView(R.id.recommedTitle)
    TextView recommedTitle;

    public RecommendHeaderHolder(Context context, ViewGroup parent, BaseAdapterRV<String> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_recommend_header);
    }



    @Override
    protected void onRefreshView(String bean, int position) {
        recommedTitle.setText(bean);
    }
}

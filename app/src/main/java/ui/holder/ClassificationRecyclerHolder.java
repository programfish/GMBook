package ui.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.CategoryList;
import presenter.Presenter;
import ui.activity.ConditionalsBooksActivity;

/**
 * Created by Administrator on 2017/12/21.
 */

public class ClassificationRecyclerHolder extends BaseHolderRV<CategoryList.MaleBean> {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.tv_mingzhi)
    TextView tvMingzhi;
    @BindView(R.id.tv_quantity)
    TextView tvQuantity;


    public ClassificationRecyclerHolder(Context context, ViewGroup parent, BaseAdapterRV<CategoryList.MaleBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_classificationrecycler);
    }

    @Override
    public void onFindViews(View itemView) {
        ButterKnife.bind(this, itemView);
    }

    @Override
    protected void onRefreshView(CategoryList.MaleBean bean, int position) {
        Presenter.getInstance().loadImage(bean.bookCover.get(0),image);
        tvMingzhi.setText(bean.name);
        tvQuantity.setText(bean.bookCount+"本");
    }

    @Override
    protected void onItemClick(View itemView, int position, CategoryList.MaleBean bean) {
        Intent intent=new Intent(context,ConditionalsBooksActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constant.KEY_TYPE,bean.name);
        intent.putExtra(Constant.KEY_GENDER,bean.gender);
        context.startActivity(intent);
    }
}

package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import base.Global;
import butterknife.BindView;
import model.httpbean.SearchDetail;
import presenter.Presenter;
import ui.activity.BookDetailedActivity;

/**
 * Created by PENG on 2017/12/30.
 */

public class SearchResultHolder extends BaseHolderRV<SearchDetail.SearchBooks> {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.shortIntro)
    TextView shortIntro;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.category)
    TextView category;

    public SearchResultHolder(Context context, ViewGroup parent, BaseAdapterRV<SearchDetail.SearchBooks> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_searchresult);
    }

    @Override
    protected void onRefreshView(SearchDetail.SearchBooks bean, int position) {
        Presenter.getInstance()
                .loadImage(bean.cover,image);
        title.setText(bean.title);
        shortIntro.setText(bean.shortIntro);
        author.setText(bean.author);
        category.setText(bean.cat);
    }

    @Override
    protected void onItemClick(View itemView, int position, SearchDetail.SearchBooks bean) {
        Constant.BOOK_NAME=bean.title;
        Constant.BOOK_ID=bean._id;
        Global.startActivity(BookDetailedActivity.class);
    }
}

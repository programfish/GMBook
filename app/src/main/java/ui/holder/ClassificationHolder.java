package ui.holder;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.CategoryList;
import model.httpbean.ClassificationBean;
import ui.adapter.ClassificationRecyclerAdapter;

/**
 * Created by Administrator on 2017/12/26.
 */

public class ClassificationHolder extends BaseHolderRV<ClassificationBean> {

    @BindView(R.id.recommedTitle)
    TextView title;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    public ClassificationHolder(Context context, ViewGroup parent, BaseAdapterRV<ClassificationBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_classification);
    }

    @Override
    public void onFindViews(View itemView) {
        ButterKnife.bind(this, itemView);
    }



    @Override
    protected void onRefreshView(ClassificationBean bean, int position) {
       title.setText(bean.getTitle());
        for(CategoryList.MaleBean maleBean:bean.data){
            switch (bean.getTitle()){
                case "男生频道":
                    maleBean.gender= Constant.BOOKS_TYPE_MALE;
                    break;
                case "女生频道":
                    maleBean.gender=Constant.BOOKS_TYPE_FEMALE;
                    break;
                case "picture":
                    maleBean.gender=Constant.BOOKS_TYPE_PICTURE;
                    break;
                case "press":
                    maleBean.gender=Constant.BOOKS_TYPE_PRESS;
                    break;
            }
        }
        ClassificationRecyclerAdapter adapter=new ClassificationRecyclerAdapter(context,bean.getData());
        recycler.setLayoutManager(new GridLayoutManager(context,2));
        recycler.setAdapter(adapter);

    }
}

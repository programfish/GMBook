package ui.holder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import base.Global;
import butterknife.BindView;
import model.httpbean.BooksByCats;
import presenter.Presenter;
import ui.activity.BookDetailedActivity;

/**
 * Created by PENG on 2018/1/1.
 */

public class ConditionalsBooksHolder extends BaseHolderRV<BooksByCats.BooksBean> {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.shortIntro)
    TextView shortIntro;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.category)
    TextView category;

    public ConditionalsBooksHolder(Context context, ViewGroup parent, BaseAdapterRV<BooksByCats.BooksBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_conditionalsbooks);
    }

    @Override
    protected void onRefreshView(BooksByCats.BooksBean bean, int position) {
        Presenter.getInstance().loadImage(bean.cover,image);
        title.setText(bean.title);
        shortIntro.setText(bean.shortIntro);
        author.setText(bean.author);
        category.setText(bean.minorCate);
        if(TextUtils.isEmpty(bean.minorCate)){
            category.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onItemClick(View itemView, int position, BooksByCats.BooksBean bean) {
        Constant.BOOK_ID=bean._id;
        Constant.BOOK_NAME=bean.title;
        Global.startActivity(BookDetailedActivity.class);
    }
}

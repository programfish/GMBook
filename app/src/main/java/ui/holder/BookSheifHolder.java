package ui.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import base.Global;
import butterknife.BindView;
import model.dbbean.BookShelf;
import presenter.Presenter;
import ui.activity.BookDetailedActivity;
import ui.activity.MainActivity;
import ui.fragment.BookShelfFragment;
import util.dialogutil.SimpleDialogFragment;
import util.dialogutil.SimpleDialogOnClickListener;

/**
 * Created by PENG on 2017/12/23.
 */

public class BookSheifHolder extends BaseHolderRV<BookShelf> {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.tv_updatetime)
    TextView tvUpdatetime;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.tv_liangzai)
    TextView tvLiangzai;

    public BookSheifHolder(Context context, ViewGroup parent, BaseAdapterRV<BookShelf> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_bookshelf);
    }


    @Override
    protected void onRefreshView(BookShelf bean, int position) {
        Presenter.getInstance()
                .loadImage(bean.getCover(), image);
        title.setText(bean.getTitle());
        tvUpdatetime.setText(Presenter.getInstance().bookUpdateTime(bean.getUpdated()));
        author.setText(bean.getAuthor() + " 著| " + Presenter.getInstance().bookReadProgreen(bean.getTitle(), bean.getChapterCount()));
        tvLiangzai.setText("连载至：" + bean.getLastChapter());
    }

    @Override
    protected void onItemClick(View itemView, int position, BookShelf bean) {
        BookShelf bookShelf = adapter.listData.get(position);
        Constant.BOOK_ID = bookShelf.getBook_id();
        Constant.BOOK_NAME = bookShelf.getTitle();
        bookShelf.setReadTiem(System.currentTimeMillis());
        Presenter.getInstance().saveData(bookShelf, "读取书籍时间");
        Global.startActivity(BookDetailedActivity.class);
    }

    @Override
    protected void onItemLong(View itemView, int position, final BookShelf bean) {

        SimpleDialogFragment.Builder builder = new SimpleDialogFragment.Builder(((MainActivity)context).getSupportFragmentManager(), "");
        builder.setMessage(bean.getTitle()+"是否移出书架？", 16, Color.BLUE)
                .setDetermine("移出", 20, Color.RED)
                .setCancel("取消")
                .setOnDetermineListener(new SimpleDialogOnClickListener() {
                    @Override
                    public void onClick(SimpleDialogFragment dialogFragment, View view) {
                        Presenter.getInstance()
                                .delete(BookShelf.class, "title=?", bean.getTitle());
                        BookShelfFragment fragment = ((MainActivity) context).getBookShelfFragment();
                        fragment.onRefreshBookSheif();
                        dialogFragment.dismiss();
                    }
                })
                .setCancelable(true)
                .build()
                .showDialog();

    }


}

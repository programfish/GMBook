package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import base.Global;
import butterknife.BindView;
import model.httpbean.Rankings;
import presenter.Presenter;
import ui.activity.BookDetailedActivity;

/**
 * Created by PENG on 2017/12/20.
 */

public class RankingListHolder extends BaseHolderRV<Rankings.RankingBean.BooksBean> {
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.shortIntro)
    TextView shortIntro;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.index)
    TextView index;

    public RankingListHolder(Context context, ViewGroup parent, BaseAdapterRV<Rankings.RankingBean.BooksBean> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_rankinglist);
    }



    @Override
    protected void onRefreshView(Rankings.RankingBean.BooksBean bean, int position) {
        Presenter.getInstance().loadImage(bean.cover,image);
        title.setText(bean.title);
        author.setText(bean.author);
        shortIntro.setText(bean.shortIntro);
        category.setText(bean.majorCate);
        index.setText(position+1+"");
    }

    @Override
    protected void onItemClick(View itemView, int position,Rankings.RankingBean.BooksBean bean ) {
        Constant.BOOK_ID=bean._id;
        Constant.BOOK_NAME=bean.title;
        Global.startActivity(BookDetailedActivity.class);
    }
}

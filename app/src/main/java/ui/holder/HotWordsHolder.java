package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import ui.activity.SearchActivity;

/**
 * Created by Administrator on 2017/12/22.
 */

public class HotWordsHolder extends BaseHolderRV<String> {

    @BindView(R.id.text)
    TextView text;

    public HotWordsHolder(Context context, ViewGroup parent, BaseAdapterRV<String> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_hotwords);
    }

    @Override
    public void onFindViews(View itemView) {
        ButterKnife.bind(this,itemView);
    }


    @Override
    protected void onRefreshView(String bean, int position) {
        text.setText(bean);
        int index=Global.randomNumber(Global.colorList.size());
        text.setBackgroundColor(Global.colorList.get(index));
    }

    @Override
    protected void onItemClick(View itemView, int position, String bean) {
        SearchActivity activity= (SearchActivity) context;
        activity.search(bean);
    }
}

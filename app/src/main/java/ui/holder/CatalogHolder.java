package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterLV;
import base.BaseHolderLV;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.BookRead;


public class CatalogHolder extends BaseHolderLV<BookRead.MixToc.Chapters> {


    @BindView(R.id.title)
    TextView title;

    public CatalogHolder(Context context, ViewGroup parent, BaseAdapterLV<BookRead.MixToc.Chapters> adapter, int position, BookRead.MixToc.Chapters bean) {
        super(context, parent, adapter, position, bean);
    }

    @Override
    public View onCreateView(Context context, ViewGroup parent) {
        View view = Global.inflate(R.layout.item_catalog, parent);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    protected void onRefreshView(BookRead.MixToc.Chapters bean, int position) {
        title.setText(bean.title);
    }

}

package ui.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import java.util.HashMap;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import ui.activity.MainActivity;
import ui.adapter.MainTabAdapter;

/**
 * Created by PENG on 2017/12/14.
 */

public class MainTabHolder extends BaseHolderRV<HashMap<String, Object>> {
    @BindView(R.id.tab_icon)
    ImageView tabIcon;
    @BindView(R.id.tab_name)
    TextView tabName;
    MainTabAdapter adapter;

    public MainTabHolder(Context context, ViewGroup parent, BaseAdapterRV<HashMap<String, Object>> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_tab);
        this.adapter= (MainTabAdapter) adapter;
    }

    @Override
    public void onFindViews(View itemView) {
        ButterKnife.bind(this, itemView);
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        params.width= (int) (Global.mScreenWidth/3);
    }

    @Override
    protected void onRefreshView(HashMap<String, Object> bean, int position) {
        tabIcon.setImageDrawable(context.getResources().getDrawable((Integer) bean.get(Constant.HASHMAP_KEY_IMAGE)));
        int size ;
        int textColor = 0;
        if (position == adapter.getCurrentPosition()) {
            // 选中的选项高亮显示
            textColor = R.color.red;
            tabName.setVisibility(View.GONE);
            tabIcon.setSelected(true);
            size = Global.dp2px(28);
        } else { // 普通的列表项
            tabName.setVisibility(View.VISIBLE);
            textColor = R.color.darkGrey;
            tabIcon.setSelected(false);
            size = Global.dp2px(20);
        }

        ViewGroup.LayoutParams layoutParams = tabIcon.getLayoutParams();
        layoutParams.height = size;
        layoutParams.width = size;
        tabName.setTextColor(Global.getColor(textColor));
        tabName.setText((CharSequence) bean.get(Constant.HASHMAP_KEY_TEXT));
    }

    @Override
    protected void onItemClick(View itemView, int position, HashMap<String, Object> bean) {
        adapter.checkPosition(position);
        ((MainActivity)context).switchFragment(position);
    }
}

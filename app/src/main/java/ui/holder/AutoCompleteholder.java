package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import butterknife.BindView;
import ui.activity.SearchActivity;

/**
 * Created by Administrator on 2017/12/29.
 */

public class AutoCompleteholder extends BaseHolderRV<String> {

    @BindView(R.id.content)
    TextView content;

    public AutoCompleteholder(Context context, ViewGroup parent, BaseAdapterRV<String> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_autocomplete);
    }

    @Override
    protected void onRefreshView(String bean, int position) {
        content.setText(bean);
    }

    @Override
    protected void onItemClick(View itemView, int position, String bean) {
        SearchActivity activity= (SearchActivity) context;
        activity.search(bean);
    }
}

package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import butterknife.BindView;
import ui.activity.ConditionalsBooksActivity;

/**
 * Created by PENG on 2018/1/1.
 */

public class ConditionsHolder extends BaseHolderRV<String> {

    @BindView(R.id.text)
    TextView text;

    public ConditionsHolder(Context context, ViewGroup parent, BaseAdapterRV<String> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_condition);
    }

    @Override
    protected void onRefreshView(String bean, int position) {
        text.setText(bean);
    }

    @Override
    protected void onItemClick(View itemView, int position, String bean) {
        ConditionalsBooksActivity activity= (ConditionalsBooksActivity) context;
        switch (position){
            case ConditionalsBooksActivity.MAJOR:
                activity.scrennmajorData();
                break;
            case ConditionalsBooksActivity.MINOR:
                activity.scrennMinorData();
                break;
            case ConditionalsBooksActivity.TYPE:
                activity.scrennType();
                break;
        }
        activity.index=position;
        activity.windowPop();
    }
}

package ui.holder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import butterknife.BindView;
import model.dbbean.SearchRecord;
import ui.activity.SearchActivity;

/**
 * Created by Administrator on 2017/12/29.
 */

public class SearchRecordHolder extends BaseHolderRV<SearchRecord> {

    @BindView(R.id.content)
    TextView content;

    public SearchRecordHolder(Context context, ViewGroup parent, BaseAdapterRV<SearchRecord> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_searchrecord);
    }

    @Override
    protected void onRefreshView(SearchRecord bean, int position) {
            content.setText(bean.getRecord());
    }

    @Override
    protected void onItemClick(View itemView, int position, SearchRecord bean) {
        SearchActivity activity= (SearchActivity) context;
        activity.search(bean.getRecord());

    }
}

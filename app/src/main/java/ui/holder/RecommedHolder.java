package ui.holder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.peng.book.R;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import base.Constant;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.Recommend;
import presenter.Presenter;
import ui.activity.BookDetailedActivity;

/**
 * Created by PENG on 2017/11/28.
 */

public class RecommedHolder extends BaseHolderRV<Recommend.RecommendBooks> {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.shortIntro)
    TextView shortIntro;
    @BindView(R.id.author)
    TextView author;

    public RecommedHolder(Context context, ViewGroup parent, BaseAdapterRV<Recommend.RecommendBooks> adapter, int itemType) {
        super(context, parent, adapter, itemType, R.layout.item_recommend);
    }

    @Override
    public void onFindViews(View itemView) {
        ButterKnife.bind(this,itemView);
    }

    @Override
    protected void onRefreshView(Recommend.RecommendBooks bean, int position) {
        Presenter.getInstance().loadImage(bean.cover,image);
        title.setText(bean.title);
        category.setText(bean.majorCate);
        shortIntro.setText(bean.shortIntro);
        author.setText(bean.author);
    }

    @Override
    protected void onItemClick(View itemView, int position, Recommend.RecommendBooks bean) {
        Intent intent=new Intent(context, BookDetailedActivity.class);
        Constant.BOOK_ID=bean._id;
        Constant.BOOK_NAME=bean.title;
        context.startActivity(intent);
    }
}

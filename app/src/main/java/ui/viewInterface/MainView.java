package ui.viewInterface;

/**
 * Created by PENG on 2017/12/1.
 */

public interface MainView<T,V> {
    void getData(int type,T data);
    void getError(int type,V error);
}

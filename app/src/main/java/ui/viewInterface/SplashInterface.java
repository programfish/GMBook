package ui.viewInterface;

import android.graphics.Bitmap;

/**
 * Created by PENG on 2018/4/9.
 */

public interface SplashInterface {

    /**
     *  展示图片
     */
    void showImage(Bitmap bitmap, String linkUrl);

    void requestFail();

}

package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.RankingListHolder;

/**
 * Created by PENG on 2017/12/20.
 */

public class RankingListAdapter extends BaseAdapterRV{

    public RankingListAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new RankingListHolder(context,parent,this,viewType);
    }


}

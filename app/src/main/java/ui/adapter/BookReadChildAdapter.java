package ui.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import base.Constant;
import base.Global;
import me.biubiubiu.justifytext.library.JustifyTextView;
import model.httpbean.BookChapterBean;
import util.SharedPreUtil;

/**
 * Created by PENG on 2017/12/3.
 */

public class BookReadChildAdapter extends PagerAdapter {
    private BookChapterBean bean;
    private View[] views;
    private int currentPosition;

    public void setBean(BookChapterBean bean) {
        this.bean = bean;
    }

    public View[] getViews() {
        return views;
    }

    @Override
    public int getCount() {
        return bean == null ? 0 :bean.getStringList().size();
    }



    public BookChapterBean getBean() {
        return bean;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views[position]);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        init(position);
        initViewData(position);

        container.addView(views[position]);
        return views[position];
    }

    private void initViewData(int position) {
        View view=views[position];
        if(view==null){
            return;
        }
        //设置标题（每章节第一个页显示）与书名或标题（z左上角一直存在）
        TextView BookName=view.findViewById(R.id.bookName);
        TextView titleView = null;
        if (position==0) {
            titleView = (TextView) view.findViewById(R.id.title);
            titleView.setText(bean.getTitle());
            titleView.setVisibility(View.VISIBLE);
            BookName.setText(bean.getBookName());
        }else {
            BookName.setText(bean.getTitle());
        }

        int textSize=SharedPreUtil.getInt(Global.mContext,Constant.KEY_TEXT_SIZE,16);
        //设置文章内容
        JustifyTextView contentText = (JustifyTextView) view.findViewById(R.id.text);
        contentText.setText(bean.getStringList().get(position));
        contentText.setTextSize(textSize);

        //设置页码
        TextView pagerNumber=view.findViewById(R.id.pagerNumber);
        pagerNumber.setText(position+1+"/"+getViews().length);

        //设置颜色
        Integer color= SharedPreUtil.getInt(Global.mContext,Constant.KEY_TEXT_COLOR,R.color.textColorBlack);
        if(color!=null){
            contentText.setTextColor(Global.mContext.getResources().getColor(color));
            BookName.setTextColor(Global.mContext.getResources().getColor(color));
            if(titleView!=null){
                titleView.setTextColor(Global.mContext.getResources().getColor(color));
            }
        }
    }

    private View init(int position){
        View view;
        if(views==null){
            views=new View[bean.getStringList().size()];
        }
        if(views[position]==null){
            view=Global.inflate(R.layout.item_bookread_childviewpager, null);
            views[position]=view;
        }else {
            view=views[position];
        }
        if(Constant.readOnclickListener!=null){
            view.setOnClickListener(Constant.readOnclickListener);
        }
        return view;
    }
}

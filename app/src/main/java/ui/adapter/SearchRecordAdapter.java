package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.SearchRecordHolder;

/**
 * Created by Administrator on 2017/12/29.
 */

public class SearchRecordAdapter extends BaseAdapterRV {

    public SearchRecordAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new SearchRecordHolder(context,parent,this,viewType);
    }
}

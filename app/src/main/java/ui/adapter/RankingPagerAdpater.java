package ui.adapter;

import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.peng.book.R;

import java.util.List;

import base.Global;
import model.httpbean.Rankings;
import presenter.Presenter;
import q.rorbin.verticaltablayout.adapter.TabAdapter;
import q.rorbin.verticaltablayout.widget.ITabView;
import ui.viewInterface.MainView;
import util.LogUtil;

/**
 * Created by PENG on 2017/12/23.
 */

public class RankingPagerAdpater extends PagerAdapter  implements TabAdapter {
    private View[] views;
    private List<String> idList;
    private List<String> titleList;

    public void setNewData(List<String> idList,List<String> titleList) {
        this.idList = idList;
        views=new View[idList.size()];
        this.titleList=titleList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return views!=null?views.length:0;
    }

    @Override
    public ITabView.TabBadge getBadge(int position) {
        return null;
    }

    @Override
    public ITabView.TabIcon getIcon(int position) {
        return null;
    }

    @Override
    public ITabView.TabTitle getTitle(int position) {
        ITabView.TabTitle.Builder builder=new ITabView.TabTitle.Builder();
        builder.setContent(titleList.get(position))
                .setTextColor(Color.RED,Color.GRAY)
                .setTextSize(14);
        return builder.build();
    }

    @Override
    public int getBackground(int position) {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view=initView(position);
        RecyclerView recyclerView=view.findViewById(R.id.recycler);
        initRecycler(recyclerView,position);
        container.addView(view);
        return view;
    }

    private View initView(int position) {
        View view;
        if(views[position]==null){
            view=Global.inflate(R.layout.item_rankingpager);
            views[position]=view;
            LogUtil.d("创建数据："+position);
        }else {
            view=views[position];
            LogUtil.d("已存在数据："+position);
        }
        return view;
    }

    private void initRecycler(RecyclerView recyclerView,int position) {
        recyclerView.setLayoutManager(new LinearLayoutManager(Global.mContext));
        final RankingListAdapter adapter=new RankingListAdapter(Global.mContext,null);
        recyclerView.setAdapter(adapter);
        Presenter.getInstance().onRankings(new MainView<Rankings,String>() {

            @Override
            public void getData(int type, Rankings data) {
                adapter.setDatas(data.ranking.books);
            }

            @Override
            public void getError(int type, String error) {

            }
        },idList.get(position),position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views[position]);
    }

}

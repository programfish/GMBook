package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.ClassificationRecyclerHolder;

/**
 * Created by Administrator on 2017/12/26.
 */

public class ClassificationRecyclerAdapter extends BaseAdapterRV {

    public ClassificationRecyclerAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new ClassificationRecyclerHolder(context,parent,this,viewType);
    }
}

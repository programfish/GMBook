package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.HotWordsHolder;

/**
 * Created by Administrator on 2017/12/22.
 */

public class HotWordsAdapter extends BaseAdapterRV {

    public HotWordsAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new HotWordsHolder(context,parent,this,viewType);
    }
}

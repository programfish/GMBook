package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import model.httpbean.Recommend;
import ui.holder.RecommedHolder;
import ui.holder.RecommendHeaderHolder;

/**
 * Created by PENG on 2017/11/28.
 */

public class RecommendAdapter extends BaseAdapterRV {
    private static final int RECOMMEND_TITLE=1;
    private static final int RECOMMEND_CONTENT=2;


    @Override
    public int getItemViewType(int position) {
        if(listData.get(position) instanceof String){
            return RECOMMEND_TITLE;
        }
        if(listData.get(position) instanceof Recommend.RecommendBooks){
            return RECOMMEND_CONTENT;
        }
        throw new IllegalArgumentException("没有-RECOMMEND");
    }

    public RecommendAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {

        switch (viewType){
            case RECOMMEND_TITLE:
                return new RecommendHeaderHolder(context,parent,this,viewType);
            case RECOMMEND_CONTENT:
                return new RecommedHolder(context,parent,this,viewType);
            default:
                throw new IllegalArgumentException("没有-RECOMMEND");
        }
    }
}

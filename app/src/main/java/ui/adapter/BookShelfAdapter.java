package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.BookSheifHolder;

/**
 * Created by PENG on 2017/12/23.
 */

public class BookShelfAdapter extends BaseAdapterRV {

    public BookShelfAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new BookSheifHolder(context,parent,this,viewType);
    }
}

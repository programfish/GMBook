package ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.peng.book.R;

import java.util.List;

import base.BaseAdapterLV;
import base.BaseHolderLV;
import base.Global;
import model.httpbean.BookRead;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import ui.holder.CatalogHolder;


public class CatalogAdapter extends BaseAdapterLV<BookRead.MixToc.Chapters> implements StickyListHeadersAdapter {

    public CatalogAdapter(Context context, List listData) {
        super(context, listData);
    }


    @Override
    public BaseHolderLV<BookRead.MixToc.Chapters> createViewHolder(Context context, ViewGroup parent, BookRead.MixToc.Chapters bean, int position) {
        return new CatalogHolder(context,parent,this,position,bean);
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = Global.inflate(R.layout.item_catalog,parent);
        }
        RelativeLayout relativeLayout=convertView.findViewById(R.id.relative);
        relativeLayout.setBackgroundColor(Global.mContext.getResources().getColor(R.color.Yellowishrice));
        TextView textView = convertView.findViewById(R.id.title);
        textView.setText(listData.get(position).getBookName());

        return convertView;
    }



    @Override
    public long getHeaderId(int position) {
        return listData.get(position).id;
    }
}

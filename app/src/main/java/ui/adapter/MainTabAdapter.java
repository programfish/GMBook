package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.MainTabHolder;

/**
 * Created by PENG on 2017/12/10.
 */

public class MainTabAdapter extends BaseAdapterRV {


    public MainTabAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new MainTabHolder(context,parent,this,viewType);
    }

    private int  currentPosition=0;

    public void checkPosition(int position) {
        this.currentPosition = position;
        notifyDataSetChanged();
    }


    public int getCurrentPosition() {
        return currentPosition;
    }
}

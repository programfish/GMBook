package ui.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;

import base.Constant;
import base.Global;
import me.biubiubiu.justifytext.library.JustifyTextView;
import model.httpbean.BookChapterBean;
import model.httpbean.BookRead;
import presenter.Presenter;
import ui.view.ChildViewPager;
import ui.viewInterface.MainView;
import util.LogUtil;


/**
 * Created by PENG on 2017/12/3.
 */

public class BookReadParentAdapter extends PagerAdapter implements MainView<BookChapterBean, Integer> {
    private View[] views;
    private List<BookChapterBean> beanList;
    private List<BookRead.MixToc.Chapters> chaptersList;


    public BookReadParentAdapter() {
        chaptersList = Constant.chaptersList;
    }


    public List<BookChapterBean> getBeanList() {
        return beanList;
    }

    public View[] getViews() {
        return views;
    }

    @Override
    public int getCount() {
        return chaptersList == null ? 0 : chaptersList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = init(position);
        updata(view, position);
        container.addView(views[position]);
        return views[position];
    }

    //切换已缓存界面的字体颜色
    public void switchTextColor(int type, int color) {
        if (beanList == null || beanList.size() == 0) {
            return;
        }
        for (BookChapterBean bean : beanList) {
            View childView = views[bean.getPosition()];
            ChildViewPager chilePager = childView.findViewById(R.id.viewpager);
            BookReadChildAdapter adapter = (BookReadChildAdapter) chilePager.getAdapter();
            if (type == Constant.TYPE_COLOR) {
                if (bean.getStringList() != null) {
                    if (adapter.getViews() != null && adapter.getViews().length != 0) {
                        for (View view : adapter.getViews()) {
                            if (view != null) {
                                JustifyTextView contentText = (JustifyTextView) view.findViewById(R.id.text);
                                TextView bookName1 = view.findViewById(R.id.bookName);
                                TextView titleView = (TextView) view.findViewById(R.id.title);
                                contentText.setTextColor(Global.mContext.getResources().getColor(color));
                                bookName1.setTextColor(Global.mContext.getResources().getColor(color));
                                titleView.setTextColor(Global.mContext.getResources().getColor(color));
                            }
                        }
                    }


                }
            } else if (type == Constant.TYPE_TXETSIZE) {
//                adapter.notifyDataSetChanged();
            }

            chilePager.getAdapter().notifyDataSetChanged();
        }

    }

    //初始化数据
    public void updata(final View view, int position) {
        TextView BookName = view.findViewById(R.id.bookName);
        BookName.setText(Constant.BOOK_NAME);
        if (beanList != null && beanList.size() != 0) {
            //根据Item的位置找对应的Bean设置对应的属性
            for (BookChapterBean bean : beanList) {
                //如果对应的Bean获取对应的小说内容则进行更新
                if (bean.getPosition() == position && bean.getStringList() != null) {
                    ChildViewPager viewPager = view.findViewById(R.id.viewpager);
                    final BookReadChildAdapter childPagerAdapter = new BookReadChildAdapter();
                    childPagerAdapter.setBean(bean);
                    viewPager.setAdapter(childPagerAdapter);
                    viewPager.setVisibility(View.VISIBLE);
                    ProgressBar pb = view.findViewById(R.id.pb);
                    pb.setVisibility(View.GONE);
                    BookName.setVisibility(View.GONE);
                    LogUtil.d("ViewPager已存在数据:" + position);
                    break;
                }
            }
        }
    }

    private View init(int position) {

        if (beanList == null) {
            beanList = new ArrayList<>();
        }
        View view;
        if (views == null) {
            views = new View[chaptersList.size()];
        }
        if (views[position] == null) {
            LogUtil.d("初始化界面 位置：" + position);
            view = Global.inflate(R.layout.item_bookread_parentviewpager, null);
            views[position] = view;
            requestData(position);
        } else {
            LogUtil.d("复用界面 位置：" + position);
            view = views[position];
        }
        if (Constant.readOnclickListener != null) {
            view.setOnClickListener(Constant.readOnclickListener);
        }
        return view;
    }

    //网络请求
    private void requestData(int position) {
        if (chaptersList != null || chaptersList.size() != 0) {
            Presenter.getInstance().onChapter(this, chaptersList.get(position).getLink(), position);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views[position]);
    }

    @Override
    public void getData(int type, BookChapterBean data) {
        beanList.add(data);
        if (data.getStringList() != null) {
            updata(views[data.getPosition()], data.getPosition());
        }
    }


    private void viewGone(View v) {
        if (v.getVisibility() == View.VISIBLE) {
            v.setVisibility(View.GONE);
        }
    }

    private void viewVisible(View v) {
        if (v.getVisibility() == View.GONE) {
            v.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getError(int type, final Integer position) {
        View view = views[position];
        if (view == null) {
            return;
        }
        final ProgressBar pb = view.findViewById(R.id.pb);
        viewGone(pb);
        final LinearLayout error = view.findViewById(R.id.error);
        viewVisible(error);
        TextView button = error.findViewById(R.id.connect);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == null) {
                    return;
                }
                requestData(position);
                viewVisible(pb);
                viewGone(error);
            }
        });
    }


}



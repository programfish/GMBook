package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.SearchResultHolder;

/**
 * Created by PENG on 2017/12/30.
 */

public class SearchResultAdapter extends BaseAdapterRV{

    public SearchResultAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new SearchResultHolder(context,parent,this,viewType);
    }
}

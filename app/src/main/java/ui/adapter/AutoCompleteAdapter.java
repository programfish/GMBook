package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.AutoCompleteholder;

/**
 * Created by Administrator on 2017/12/29.
 */

public class AutoCompleteAdapter extends BaseAdapterRV {

    public AutoCompleteAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new AutoCompleteholder(context,parent,this,viewType);
    }
}

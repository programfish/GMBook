package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.ClassificationHolder;

/**
 * Created by Administrator on 2017/12/21.
 */

public class ClassificationAdapter extends BaseAdapterRV {


    public ClassificationAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new ClassificationHolder(context,parent,this,viewType);
    }
}

package ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.util.List;

import base.BaseAdapterRV;
import base.BaseHolderRV;
import ui.holder.ConditionsHolder;

/**
 * Created by PENG on 2018/1/1.
 */

public class ConditionsAdapter extends BaseAdapterRV{

    public ConditionsAdapter(Context context, List listData) {
        super(context, listData);
    }

    @Override
    public BaseHolderRV createViewHolder(Context context, ViewGroup parent, int viewType) {
        return new ConditionsHolder(context,parent,this,viewType);
    }
}

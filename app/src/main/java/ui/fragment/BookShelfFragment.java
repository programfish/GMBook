package ui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.util.List;

import base.BaseFragment;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.dbbean.BookShelf;
import presenter.Presenter;
import ui.activity.SearchActivity;
import ui.adapter.BookShelfAdapter;

/**
 * Created by PENG on 2017/12/10.
 */

public class BookShelfFragment extends BaseFragment {
    @BindView(R.id.iv_shousuo)
    ImageView ivShousuo;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    private BookShelfAdapter adapter;
    private List<BookShelf> bookShelfList;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_bookshelf;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        initRecycler();
    }


    private void initRecycler() {
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BookShelfAdapter(getContext(),null);
        recycler.setAdapter(adapter);
    }


    public void onRefreshBookSheif(){
        bookShelfList=Presenter.getInstance().selectListOrder("readTiem desc",BookShelf.class);
        adapter.setDatas(bookShelfList);
    }

    @Override
    public void onStart() {
        super.onStart();
        onRefreshBookSheif();
    }

    @Override
    public void initListener() {
        ivShousuo.setOnClickListener(this);
    }


    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.iv_shousuo:
                Global.startActivity(SearchActivity.class);
                break;
        }
    }

}

package ui.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;

import base.BaseFragment;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import model.httpbean.RankingList;
import presenter.Presenter;
import q.rorbin.verticaltablayout.VerticalTabLayout;
import q.rorbin.verticaltablayout.widget.TabView;
import ui.activity.ClassificationActivity;
import ui.adapter.RankingPagerAdpater;
import ui.viewInterface.MainView;

/**
 * Created by PENG on 2017/12/10.
 */

public class BookRankingListFragment extends BaseFragment
        implements MainView<RankingList, String> {


    @BindView(R.id.tablayout)
    VerticalTabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.classification)
    ImageView classification;
    Unbinder unbinder;



    private RankingPagerAdpater pageradpater;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_rankinglist;
    }


    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        initViewPager();
    }

    private void initViewPager() {
        pageradpater = new RankingPagerAdpater();
        viewpager.setAdapter(pageradpater);
    }


    private void initTabLayout(List<RankingList.MaleBean> maleBeanList) {
        if (maleBeanList == null || maleBeanList.size() == 0) {
            return;
        }
        List<String> idList = new ArrayList<>();
        List<String> titleList = new ArrayList<>();
        tabLayout.setupWithViewPager(viewpager);
        for (RankingList.MaleBean bean : maleBeanList) {
            titleList.add(bean.title);
            idList.add(bean._id);
        }
        pageradpater.setNewData(idList, titleList);
        updateTabLayout(idList.size());
    }

    private void updateTabLayout(int count) {
        for (int i = 0; i < count; i++) {
            TabView tabView = tabLayout.getTabAt(i);
            tabView.setPadding(10, 0, 10, 0);
        }
    }


    @Override
    public void initListener() {
        classification.setOnClickListener(this);
    }

    @Override
    public void initData() {
        Presenter.getInstance().onRankingList(this);
    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.classification:
                Global.startActivity(ClassificationActivity.class);
                break;
        }
    }


    @Override
    public void getData(int type, RankingList data) {
        initTabLayout(data.male);
    }

    @Override
    public void getError(int type, String error) {


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}




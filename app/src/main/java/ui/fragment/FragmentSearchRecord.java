package ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import java.util.List;

import base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import model.dbbean.SearchRecord;
import presenter.Presenter;
import ui.activity.SearchActivity;
import ui.adapter.SearchRecordAdapter;
import util.LogUtil;

/**
 * Created by PENG on 2017/12/29.
 */

public class FragmentSearchRecord extends BaseFragment {

    @BindView(R.id.recycler)
    RecyclerView recycler;
    Unbinder unbinder;
    @BindView(R.id.clear)
    TextView clear;
    private SearchRecordAdapter adapter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_searchrecord;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        initRecycler();
    }

    private void initRecycler() {
        adapter = new SearchRecordAdapter(getContext(), null);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);
    }

    @Override
    public void initListener() {
        clear.setOnClickListener(this);
    }

    @Override
    public void initData() {
        initSearchData(Presenter.getInstance().selectListOrder("searchTime desc", SearchRecord.class));
    }

    public void initSearchData( List<SearchRecord> searchRecordList ) {
        if(adapter==null){
            LogUtil.d("adapter==null");
            return;
        }
        if (adapter.listData != null) {
            adapter.listData.clear();
        }
        if (searchRecordList != null && searchRecordList.size() != 0) {
            adapter.setDatas(searchRecordList);
            LogUtil.d("searchRecord-new");
        } else {
            adapter.setDatas(null);
        }
    }


    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.clear:
                clear();
                break;
        }
    }

    /**
     * 1、删除数据库数据
     * 2、Recycler刷新数据
     * 3、数据库无搜素记录切换热词Fragment
     */
    private void clear() {
        Presenter.getInstance()
                .delete(SearchRecord.class);
        initSearchData(null);
        SearchActivity activity= (SearchActivity) getContext();
        activity.initEdit();
        activity.SearchRecordToHotWrods();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}

package ui.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.peng.book.R;

import java.util.List;

import base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ui.adapter.AutoCompleteAdapter;

/**
 * Created by Administrator on 2017/12/29.
 */

public class FragmentAutoComplete extends BaseFragment {
    @BindView(R.id.recycler)
    RecyclerView recycler;
    Unbinder unbinder;
    private AutoCompleteAdapter adapter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_autocomplete;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this,mRoot);
        ininReycler();
    }

    private void ininReycler() {
        adapter = new AutoCompleteAdapter(getContext(),null);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);
    }

    public void setRecyclerData(List<String> data){
        adapter.setDatas(data);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }


}

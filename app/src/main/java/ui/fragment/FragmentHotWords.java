package ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;

import base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import model.httpbean.HotWord;
import presenter.Presenter;
import ui.adapter.HotWordsAdapter;
import ui.viewInterface.MainView;
import util.LogUtil;

/**
 * Created by Administrator on 2017/12/28.
 */

public class FragmentHotWords extends BaseFragment implements MainView<HotWord, String> {
    @BindView(R.id.tv_searchforwords)
    TextView tvSearchforwords;
    @BindView(R.id.tv_Inabatch)
    TextView tvInabatch;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    Unbinder unbinder;
    private HotWordsAdapter adapter;
    private List<String> recyclerData;
    private List<List<String>> strings;
    private int index = 0;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_searchhotwords;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this,mRoot);
        initReyclerView();
    }

    private void initReyclerView() {
        recycler.setLayoutManager(new StaggeredGridLayoutManager(3,RecyclerView.HORIZONTAL));
        adapter = new HotWordsAdapter(getContext(), null);
        recycler.setAdapter(adapter);
    }


    @Override
    public void initListener() {
        tvInabatch.setOnClickListener(this);
    }

    @Override
    public void initData() {
        Presenter.getInstance()
                .onHotWords(this);

    }

    private void initRecyclerData() {
        recyclerData = new ArrayList<>();
        replaceData();
    }

    private void replaceData() {
        LogUtil.d("index--"+index);
        recyclerData=strings.get(index);
        adapter.setDatas(recyclerData);
    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.tv_Inabatch:
                if(recyclerData.size()==0||recyclerData==null){
                    return;
                }
                index++;
                if (index == strings.size()) {
                    index = 0;
                }

                replaceData();
                break;
        }
    }

    private static final int HOTWORKSLENGTH=9;

    @Override
    public void getData(int type, HotWord data) {
        List<String> dataString=data.hotWords;
        strings = new ArrayList<>();
        List<String> hotWords;
        int index=0;
        for(int i=0;i<dataString.size()/HOTWORKSLENGTH;i++){
            hotWords=new ArrayList<>();
            for(int h=0;h<HOTWORKSLENGTH;h++){
                hotWords.add(dataString.get(index));
                index++;
            }
            strings.add(hotWords);
        }
        initRecyclerData();
    }

    @Override
    public void getError(int type, String error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

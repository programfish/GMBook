package ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import base.BaseFragment;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.httpbean.Recommend;
import presenter.Presenter;
import ui.activity.ConditionalsBooksActivity;
import ui.activity.SearchActivity;
import ui.adapter.RecommendAdapter;
import ui.viewInterface.MainView;

/**
 * Created by PENG on 2017/11/28.
 */

public class BookRecommendFragment extends BaseFragment implements MainView<Recommend, String> {
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.error)
    LinearLayout error;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;
    Unbinder unbinder;
    @BindView(R.id.screen)
    ImageView screen;
    @BindView(R.id.iv_shousuo)
    ImageView ivShousuo;

    private RecommendAdapter adapter;


    @Override
    public int getLayoutRes() {
        return R.layout.fragment_recommend;
    }


    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        adapter = new RecommendAdapter(getContext(), null);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);
    }

    @Override
    public void initListener() {
        screen.setOnClickListener(this);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Presenter.getInstance().onRecommend(BookRecommendFragment.this);
                if (error.getVisibility() == View.VISIBLE) {
                    error.setVisibility(View.GONE);
                }
            }
        });
        ivShousuo.setOnClickListener(this);
    }


    @Override
    public void initData() {
        refresh.setRefreshing(true);
        Presenter.getInstance().onRecommend(this);
    }

    //初始化出版书籍
    private Recommend.RecommendBooks initPublishingBook1() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "573f07b5350b6d355d14cb66";
        book1.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F707005%2F_707005_237918.jpg%2F";
        book1.title = "西游记";
        book1.majorCate = "传记名著";
        book1.author = "（明）吴承恩　著/李伟　注释";
        book1.shortIntro = "《西游记》描写的幻想世界和神话人物，大都有现实生活作基础，同时在神奇的形态下体现了作家与人民的某些美好愿望。八十一难、七十二变、各种神魔的本领都充满幻想色彩；五花八门、奇光异彩的宝贝，显然是人们为了征服自然或战胜敌人才假想出来的。《西游记》构成了浪漫主义的基本艺术特征。";
        return book1;
    }

    private Recommend.RecommendBooks initPublishingBook2() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "536c902ad222e0dd260013f1";
        book1.cover = "/agent/http://media.tadu.com/2013/12/09/16/56/24562_f7dc7f71111b482d9d96326f991d2cb6_250_200.jpg";
        book1.title = "水浒传";
        book1.majorCate = "传记名著";
        book1.author = "施耐庵";
        book1.shortIntro = "本书是中国历史上第一部用白话文写成的章回小说，以宋江领导的起义军为主要题材，通过一系列梁山英雄反抗压迫、英勇斗争的生动故事，揭示了当时的社会矛盾，暴露了北宋末年...";
        return book1;
    }

    private Recommend.RecommendBooks initPublishingBook3() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "51c162ba53e597de280001f9";
        book1.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F53598%2F_53598_122310.jpg%2F";
        book1.title = "三国演义";
        book1.majorCate = "传记名著";
        book1.author = "罗贯中";
        book1.shortIntro = "《三国演义》中国古典四大名著之一.元末明初小说家罗贯中所著,是中国第一部长篇章回体历史演义的小说.描写了从东汉末年到西晋初年之间近100年的历史风云.全书反映了三国时代的政治军事斗争,反映了三国时代各类社会矛盾的渗透与转化,概括了这一时代的历史巨变,塑造了一批叱咤风云的英雄人物.";
        return book1;
    }

    private Recommend.RecommendBooks initPublishingBook4() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "56a07c61c79a87787e22df75";
        book1.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F2066291%2F2066291_23c41d7bfe624cf2ab43466698a26baf.jpg%2F";
        book1.title = "假如给我三天光明";
        book1.majorCate = "成功励志";
        book1.author = "海伦·凯勒";
        book1.shortIntro = "本书选取了影响最广的《假如给我三天光明》《我的一生》《我的后半生》《老师的故事》.除此之外,本书还收录了海伦·凯勒与安妮·莎立文的书信集,以及安妮·莎立文的教学报告、演讲等内容,以求把海伦·凯勒生动而伟大的一生完整系统地介绍出来.相信读者通过这些文章可以深刻了解海伦的生命轨迹.";
        return book1;
    }

    private Recommend.RecommendBooks initPublishingBook5() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "548f0d13c3ba984f251773ea";
        book1.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F43875%2F_43875_729745.jpg%2F";
        book1.title = "老人与海";
        book1.majorCate = "外文原版";
        book1.author = "海明威";
        book1.shortIntro = "《老人与海》虽然是一个故事简单、篇幅不大的作品,但含义丰富,很多教师把它作为英雄主义教育的教材,推荐给广大学生,使之成为经久不衰的畅销书.本作品一经出版就得到了评论家们一致好评,使海明威获得了1953年度的普利策奖金和1954年度的诺贝尔文学奖.";
        return book1;
    }

    private Recommend.RecommendBooks initPublishingBook6() {
        Recommend.RecommendBooks book1 = new Recommend.RecommendBooks();
        book1._id = "574fd0ed1cd40b580e45eb94";
        book1.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F7127%2F_7127_645187.jpg%2F";
        book1.title = "双城记";
        book1.majorCate = "人文社科";
        book1.author = "(英) 狄更斯";
        book1.shortIntro = "《双城记》是英国文豪狄更斯作品中故事情节最曲折惊险、最惊心动魄的一部。小说以法国大革命为背景，以巴黎和伦敦作为故事的发生地。讲述了法国贵族埃弗瑞 蒙德侯爵兄弟为了霸占一美貌农妇，几乎杀死其全家。医生马奈特向当局告发此事，却反被侯爵兄弟陷害，身陷囹圄18年。出狱后，精神失常的马奈特被女儿露西 接回。当时达内和卡顿都在追求露西，她选择了达内。法国大革命中，达内因受到德发日夫妇的控告而被捕并判处死刑。露西去法国营救丈夫，未果。此时，卡顿冒 名顶替救出达内，马奈特一家秘密返回英国。";
        return book1;
    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.iv_shousuo:
                Global.startActivity(SearchActivity.class);
                break;
            case R.id.screen:
                Intent intent=new Intent(getContext(),ConditionalsBooksActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.KEY_TYPE,"玄幻");
                intent.putExtra(Constant.KEY_GENDER,"male");
                startActivity(intent);
                break;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getData(int type, Recommend books) {
        if (adapter.listData != null) {
            adapter.listData.clear();
        }
        List<Recommend.RecommendBooks> booksList1 = new ArrayList<>();
        List<Recommend.RecommendBooks> booksList2 = new ArrayList<>();
        for (int i = 0; i < books.books.size(); i++) {
            if (i >= books.books.size() / 2) {
                booksList2.add(books.books.get(i));
                continue;
            }
            booksList1.add(books.books.get(i));
        }
        adapter.addData("传记名著");
        adapter.addData(initPublishingBook1());
        adapter.addData(initPublishingBook2());
        adapter.addData(initPublishingBook3());
        adapter.addData("国外书籍");
        adapter.addData(initPublishingBook4());
        adapter.addData(initPublishingBook5());
        adapter.addData(initPublishingBook6());
        adapter.addData("每日精选");
        adapter.addDatas(booksList1);
        adapter.addData("主编力荐");
        adapter.addDatas(booksList2);
        delayRefresh();
        recycler.setVisibility(View.VISIBLE);
    }

    public void delayRefresh() {
        Flowable.timer(1, TimeUnit.SECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        refresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public void getError(int type, String error) {
        delayRefresh();
        if (recycler.getVisibility() == View.VISIBLE) {
            this.error.setVisibility(View.GONE);
        } else {

            this.error.setVisibility(View.VISIBLE);
        }
        Toast.makeText(getContext(), "请求网络失败，下拉刷新更多", Toast.LENGTH_SHORT).show();
    }
}

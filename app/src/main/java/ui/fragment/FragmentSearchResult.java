package ui.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.peng.book.R;

import base.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import model.httpbean.SearchDetail;
import presenter.Presenter;
import ui.adapter.SearchResultAdapter;
import ui.viewInterface.MainView;

/**
 * Created by PENG on 2017/12/30.
 */

public class FragmentSearchResult extends BaseFragment implements MainView<SearchDetail, String> {
    @BindView(R.id.recycler)
    RecyclerView recycler;
    Unbinder unbinder;
    @BindView(R.id.error)
    LinearLayout error;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;
    private SearchResultAdapter adapter;
    private static String searchName;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_searchresult;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this, mRoot);
        initRecycler();
    }

    private void initRecycler() {
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new SearchResultAdapter(getContext(), null);
        recycler.setAdapter(adapter);
    }

    public void requestData(String name) {
        Presenter.getInstance()
                .onSearchBooks(this, name);
        searchName=name;
        if(refresh!=null){
            refresh.setRefreshing(true);
        }

    }

    @Override
    public void initListener() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                error.setVisibility(View.GONE);
                requestData(searchName);
            }
        });
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getData(int type, SearchDetail data) {
        adapterDataClear();
        adapter.setDatas(data.books);
        refresh.setRefreshing(false);
    }

    private void adapterDataClear(){
        if (adapter.listData != null) {
            adapter.listData.clear();
        }
    }

    @Override
    public void getError(int type, String error) {
        adapterDataClear();
        refresh.setRefreshing(false);
        this.error.setVisibility(View.VISIBLE);
    }
}

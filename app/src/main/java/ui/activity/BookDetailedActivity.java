package ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.peng.book.R;
import com.hedgehog.ratingbar.RatingBar;

import java.util.concurrent.TimeUnit;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.dbbean.BookShelf;
import model.dbbean.ReadProgress;
import model.httpbean.BookDetail;
import presenter.Presenter;
import ui.viewInterface.MainView;

import static base.Constant.chaptersList;


public class BookDetailedActivity extends BaseActivity implements MainView<BookDetail, String> {


    @BindView(R.id.headerLayout)
    LinearLayout headerLayout;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.btn_more)
    ImageView btnMore;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.authorLevel)
    TextView authorLevel;
    @BindView(R.id.ratingbar)
    RatingBar ratingbar;
    @BindView(R.id.cat)
    TextView cat;
    @BindView(R.id.wordCount)
    TextView wordCount;
    @BindView(R.id.longIntro)
    TextView longIntro;
    @BindView(R.id.latelyFollower)
    TextView latelyFollower;
    @BindView(R.id.retentionRatio)
    TextView retentionRatio;
    @BindView(R.id.serializeWordCount)
    TextView serializeWordCount;
    @BindView(R.id.intro)
    TextView intro;
    @BindView(R.id.showLongIntro)
    ImageView showLongIntro;
    @BindView(R.id.chapterCountAndUpdataTime)
    TextView chapterCountAndUpdataTime;
    @BindView(R.id.btn_catalog)
    LinearLayout btnCatalog;
    @BindView(R.id.read)
    TextView read;
    @BindView(R.id.addBookShelf)
    TextView addBookShelf;

    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;

    private Intent intent;
    private static final String EXISTENCE = "已加书架";
    private static final String NO_EXISTENCE = "加入书架";
    private BookShelf bookShelf;


    @Override
    public int getLayoutRes() {
        Global.setNoStatusBarFullMode(this);
        return R.layout.activity_book_detailed;
    }

    @Override
    public void initView() {
        chaptersList = null;
        ButterKnife.bind(this);
        initLayout();
        initLongIntro();
        initAddBookShelf();
    }


    private void initAddBookShelf() {
        bookShelf = Presenter.getInstance().selectFirst(BookShelf.class, "title=?", Constant.BOOK_NAME);
        if (bookShelf != null) {
            addBookShelf.setText(EXISTENCE);
            addBookShelf.setTextColor(Color.GRAY);
            addBookShelf.setEnabled(false);
        } else {
            addBookShelf.setTextColor(getResources().getColor(R.color.textColorBlack));
            addBookShelf.setText(NO_EXISTENCE);
        }
    }

    private void initLayout() {
        int statusBarHeight = Global.getStatusBarHeight(this);
        headerLayout.setPadding(0, statusBarHeight, 0, 0);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) headerLayout.getLayoutParams();
        params.height = Global.dp2px(170) + statusBarHeight;
    }


    @Override
    public void initData() {
        refresh.setRefreshing(true);
        Presenter.getInstance().onBookDetail(BookDetailedActivity.this, Constant.BOOK_ID);
    }

    private void initLongIntro() {
        longIntro.post(new Runnable() {
            @Override
            public void run() {
                Constant.LONGINTRO_HIGH = longIntro.getLineHeight();
                int height = Constant.LONGINTRO_HIGH * 3;
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) longIntro.getLayoutParams();
                params.height = height;
            }
        });
    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.btn_more:
                break;
            case R.id.longIntro:
                isOnClickLongIntro();
                break;
            case R.id.btn_catalog:
                if (intent != null) {
                    startActivity(intent);
                }
                break;
            case R.id.addBookShelf:
                addBookShelf();
                break;
            case R.id.read:
                read();
                break;
        }
    }


    //立即阅读
    private void read() {
        Intent intent = new Intent(getApplicationContext(), BookReadActivity.class);
        //Constant-BookName和BookId不能为空
        ReadProgress readProgress =
                Presenter
                        .getInstance()
                        .selectFirst(ReadProgress.class, "bookName=?", Constant.BOOK_NAME);
        if (readProgress == null) {
            readProgress = new ReadProgress();
            readProgress.setPosition(0);
            Presenter.getInstance().saveData(readProgress, Constant.BOOK_NAME + "详情页面进度数据");
        }
        intent.putExtra(Constant.KEY_POSITION, readProgress.getPosition());
        startActivity(intent);
    }

    //加入书架

    public void addBookShelf() {
        if (addBookShelf.getText().toString().equals(NO_EXISTENCE)) {
            if (bookShelf != null) {
                Presenter.getInstance().saveData(bookShelf, "加入书架");
                initAddBookShelf();
            } else {
                Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void initListener() {
        btnMore.setOnClickListener(this);
        longIntro.setOnClickListener(this);
        btnCatalog.setOnClickListener(this);
        read.setOnClickListener(this);
        addBookShelf.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Presenter.getInstance().onBookDetail(BookDetailedActivity.this, Constant.BOOK_ID);
            }
        });
    }


    private boolean isLongIntro = false;

    private void isOnClickLongIntro() {
        if (longIntro.getText().toString() == null) {
            return;
        }
        if (longIntro.getLineCount() > 3) {
            int height = 0;
            if (isLongIntro) {
                height = Constant.LONGINTRO_HIGH * 3;
                showLongIntro.setVisibility(View.VISIBLE);
                isLongIntro = false;
            } else {
                height = Constant.LONGINTRO_HIGH * longIntro.getLineCount();
                showLongIntro.setVisibility(View.GONE);
                isLongIntro = true;
            }
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) longIntro.getLayoutParams();
            params.height = height;
            longIntro.setLayoutParams(params);
        }
    }


    @Override
    public void getData(int type, BookDetail bookDetail) {
        if (bookDetail._id.equals("56a07c61c79a87787e22df75")) {
            bookDetail.cover = "/agent/http%3A%2F%2Fimg.1391.com%2Fapi%2Fv1%2Fbookcenter%2Fcover%2F1%2F2066291%2F2066291_23c41d7bfe624cf2ab43466698a26baf.jpg%2F";
        }
        Presenter.getInstance().loadImage(bookDetail.cover, image);
        title.setText(bookDetail.title);
        author.setText(bookDetail.author);
        String strCat;
        strCat = bookDetail.majorCate;
        if (!TextUtils.isEmpty(bookDetail.minorCate)) {
            strCat += "/" + bookDetail.minorCate;
        }
        cat.setText(strCat);
        String countNumber = "";
        int wordNumber = bookDetail.wordCount;
        if (wordNumber < 10000) {
            countNumber = wordNumber + "字";
        } else {
            countNumber = ((int) wordNumber / 10000) + "万字";
        }
        wordCount.setText(countNumber);
        longIntro.setText(bookDetail.longIntro);
        latelyFollower.setText("追书人数\n" + bookDetail.latelyFollower);
        retentionRatio.setText("保留率\n" + bookDetail.retentionRatio + "%");
        serializeWordCount.setText("日更新字数\n" + bookDetail.serializeWordCount);
        longIntro.post(new Runnable() {
            @Override
            public void run() {
                if (longIntro.getLineCount() <= 3) {
                    showLongIntro.setVisibility(View.GONE);
                }
            }
        });

        String time = bookDetail.updated.substring(0, bookDetail.updated.indexOf("T"));
        String updatTime = "连载至" + bookDetail.chaptersCount + "章\t更新于" + time;
        chapterCountAndUpdataTime.setText(updatTime);
        intent = new Intent(this, BookCatalogActivity.class);
        intent.putExtra(Constant.KEY_ID, bookDetail._id);
        Constant.BOOK_NAME = bookDetail.title;

        bookShelf = Presenter
                .getInstance()
                .selectFirst(BookShelf.class, "title=?", bookDetail.title);
        boolean isNull;
        if (bookShelf != null) {
            isNull = true;
        } else {
            bookShelf = new BookShelf();
            isNull = false;
        }
        bookShelf.setAuthor(bookDetail.author);
        bookShelf.setBook_id(bookDetail._id);
        bookShelf.setCover(bookDetail.cover);
        bookShelf.setLastChapter(bookDetail.lastChapter);
        bookShelf.setUpdated(bookDetail.updated);
        bookShelf.setTitle(bookDetail.title);
        bookShelf.setChapterCount(bookDetail.chaptersCount);
        if (isNull) {
            Presenter.getInstance().saveData(bookShelf, "更新书籍");
        }
        refresh.setRefreshing(false);
    }

    public void delayRefresh(){
        Flowable.timer(1, TimeUnit.SECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        refresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public void getError(int type, String error) {
        delayRefresh();
        Toast.makeText(this, "网络请求失败，下拉刷新更多", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}

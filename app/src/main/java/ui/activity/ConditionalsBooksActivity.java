package ui.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.peng.book.R;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.BooksByCats;
import model.httpbean.CategoryListLv2;
import presenter.Presenter;
import ui.adapter.ConditionalsBooksAdapter;
import ui.adapter.ConditionsAdapter;
import ui.adapter.ScreenAdapter;
import ui.viewInterface.MainView;
import util.LogUtil;

/**
 * Created by PENG on 2018/1/1.
 */

public class ConditionalsBooksActivity extends BaseActivity implements MainView<Object, String>,XRecyclerView.LoadingListener {
    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.xrecycler)
    public XRecyclerView xrecycler;

    public String gender;
    public String type;
    public String major;
    public String minor;
    public int start;
    public int limit;

    public ConditionalsBooksAdapter booksAdapter;
    public PopupWindow window;
    @BindView(R.id.title)
    TextView title;
    private View contentView;
    private RecyclerView scrennRecycler;
    private ScreenAdapter screenAdapter;
    private CategoryListLv2 categoryListLv2;
    public int index;
    public static final int MAJOR = 0;
    public static final int MINOR = 1;
    public static final int TYPE = 2;
    public ConditionsAdapter adapter;
    private PopupMenu popupMenu;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_conditionalsbooks;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        Global.setStatusBarColor(this, Global.getColor(R.color.red2));
        initCategoryListLv2();
        initConditionsRecycler();
        initXRecycler();
        initPopWindow();
        initPopMenu();
    }

    //请求二级分类
    private void initCategoryListLv2() {
        Presenter.getInstance()
                .onCategoryListLv2(this);
    }

    //初始化XRecycler
    private void initXRecycler() {
        booksAdapter = new ConditionalsBooksAdapter(getApplicationContext(), null);
        xrecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        xrecycler.setAdapter(booksAdapter);
        xrecycler.setLoadingMoreEnabled(true);
        xrecycler.loadMoreComplete();
        xrecycler.setLoadingMoreProgressStyle(ProgressStyle.SquareSpin);
        xrecycler.refreshComplete();
        xrecycler.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        xrecycler.setLoadingListener(this);
    }


    public List<String> typeList;

    //初始化条件recycler
    private void initConditionsRecycler() {
        Intent intent = getIntent();
        major = intent.getStringExtra(Constant.KEY_TYPE);
        gender = intent.getStringExtra(Constant.KEY_GENDER);
        title.setText(gender);
        recycler.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        List<String> typeData = new ArrayList<>();
        typeData.add(major);
        typeData.add("全部");
        typeData.add("热门");
        adapter = new ConditionsAdapter(this, typeData);
        recycler.setAdapter(adapter);
        start = 0;
        limit = 50;
        typeList = new ArrayList<>();
        typeList.add("hot");
        typeList.add("new");
        typeList.add("reputation");
        typeList.add("over");
        type = typeList.get(0);
    }

    //初始化弹出菜单
    private void initPopMenu(){
        popupMenu = new PopupMenu(this, title);
        popupMenu.inflate(R.menu.menu_conditionals);

    }

    //初始化弹出筛选条件
    private void initPopWindow() {
        window = new PopupWindow(getApplicationContext());
        window.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        contentView = Global.inflate(R.layout.pop_screen);
        scrennRecycler = contentView.findViewById(R.id.recycler);
        scrennRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        screenAdapter = new ScreenAdapter(this, null);
        scrennRecycler.setAdapter(screenAdapter);
        window.setContentView(contentView);
        window.setBackgroundDrawable(new ColorDrawable());
        window.setFocusable(true);
        window.setOutsideTouchable(true);
    }

    //根据gender返回下级筛选数据
    private List<CategoryListLv2.MaleBean> scrennData() {
        if (categoryListLv2 == null) {
            return null;
        }
        List<CategoryListLv2.MaleBean> bean = new ArrayList<>();
        switch (gender) {
            case Constant.BOOKS_TYPE_MALE:
                bean = categoryListLv2.male;
                break;
            case Constant.BOOKS_TYPE_FEMALE:
                bean = categoryListLv2.female;
                break;
            case Constant.BOOKS_TYPE_PICTURE:
                bean=categoryListLv2.picture;
                break;
            case Constant.BOOKS_TYPE_PRESS:
                bean=categoryListLv2.press;
                break;
        }
        return bean;
    }

    //根据major筛选mins
    public void scrennMinorData() {
        List<CategoryListLv2.MaleBean> beanList = scrennData();
        for (CategoryListLv2.MaleBean maleBean : beanList) {
            if (maleBean.major.equals(major)) {
                List<String> list = new ArrayList<>();
                list.add("全部");
                list.addAll(maleBean.mins);
                screenAdapter.setDatas(list);
                return;
            }
        }
    }

    private List<String> scrennType;

    //初始化type
    public void scrennType() {
        if (scrennType == null) {
            scrennType = new ArrayList<>();
            scrennType.add("热门");
            scrennType.add("新书");
            scrennType.add("热评");
            scrennType.add("完结");
        }
        screenAdapter.setDatas(scrennType);
    }

    //筛选Gender 获取对应一个major mins默认为全部
    private void screenGender(){
        List<CategoryListLv2.MaleBean> beanList=scrennData();
        adapter.listData.set(0,beanList.get(0).major);
        adapter.listData.set(1,"全部");
        adapter.notifyDataSetChanged();
        booksAdapter.listData.clear();
        booksAdapter.notifyDataSetChanged();
        major= (String) adapter.listData.get(0);;
        minor=null;
        xrecycler.refresh();
    }

    public void windowPop() {
        window.showAsDropDown(recycler);
    }

    //根据gender获取major筛选的数据
    public void scrennmajorData() {
        List<CategoryListLv2.MaleBean> beanList = scrennData();
        List<String> genderList = new ArrayList<>();
        for (CategoryListLv2.MaleBean maleBean : beanList) {
            genderList.add(maleBean.major);
        }
        screenAdapter.setDatas(genderList);
    }


    @Override
    public void initListener() {
        title.setOnClickListener(this);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                gender=item.getTitle().toString();
                title.setText(gender);
                screenGender();
                return true;
            }
        });
    }

    public void requestData() {
        Presenter.getInstance()
                .onConditionalBooks(this, gender, type, major, minor, start, limit);
        LogUtil.d("gender--"+gender+"--type--"+type+"--major--"+major+"--minor--"+minor);
    }

    @Override
    public void initData() {
        xrecycler.refresh();
    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.title:
                popupMenu.show();
                break;
        }
    }




    @Override
    public void getData(int type, Object data) {
        switch (type) {
            case Constant.HTTP_TYPE_CONDITIONALSBOOKS:
                BooksByCats cats = (BooksByCats) data;
                booksAdapter.addDatas(cats.books);
                xrecycler.loadMoreComplete();
                break;
            case Constant.HTTP_TYPE_CATEGORYLISTLV2:
                categoryListLv2 = (CategoryListLv2) data;
                break;
        }
        xrecycler.refreshComplete();

    }

    @Override
    public void getError(int type, String error) {
        Toast.makeText(this, "加载数据失败，请重试", Toast.LENGTH_SHORT).show();
        xrecycler.loadMoreComplete();
        xrecycler.refreshComplete();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (xrecycler != null) {
            xrecycler.destroy();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onRefresh() {
        start=0;
        if(booksAdapter.listData!=null){
            booksAdapter.listData.clear();
            booksAdapter.notifyDataSetChanged();
        }

        requestData();
    }

    @Override
    public void onLoadMore() {
        start += limit;
        requestData();
    }
}

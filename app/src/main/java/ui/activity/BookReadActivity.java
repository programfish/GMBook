package ui.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.peng.book.R;

import java.util.List;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.biubiubiu.justifytext.library.JustifyTextView;
import model.dbbean.ReadProgress;
import model.httpbean.BookChapterBean;
import model.httpbean.BookRead;
import presenter.Presenter;
import ui.adapter.BookReadParentAdapter;
import ui.view.ParentViewPager;
import ui.viewInterface.MainView;
import util.LogUtil;
import util.SharedPreUtil;

import static util.SharedPreUtil.getBoolean;

public class BookReadActivity extends BaseActivity implements MainView<BookRead,String>{


    @BindView(R.id.viewpager)
    ParentViewPager viewpager;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.text)
    JustifyTextView text;
    @BindView(R.id.layout_book_read)
    LinearLayout layoutBookRead;
    @BindView(R.id.bookName)
    TextView bookName;
    @BindView(R.id.pb)
    ProgressBar pb;
    @BindView(R.id.connect)
    TextView connect;
    @BindView(R.id.error)
    LinearLayout error;


    private BookReadParentAdapter viewPagerAdapter;
    public static int firstLineNumber;
    public static int otherLineNumber;
    public static int aLineWidth;
    private static final String MODE_NIGHT = "夜间模式";
    private static final String MODE_DEFAULT = "日间模式";
    private PopupWindow readWindow;
    private boolean readState;
    private View readView;
    private TextView btn_catalog;
    private TextView btn_mode;
    private SeekBar progress;
    private TextView progressText;
    private TextView setTextView;
    private PopupWindow setTextSizeWindow;
    private TextView setValue;
    private TextView setAdd;
    private TextView setMinus;
    private List<BookRead.MixToc.Chapters> chapterses;

    @Override
    public int getLayoutRes() {
        Global.setNoStatusBarFullMode(this);
        return R.layout.activity_book_read;
    }


    @Override
    public void initView() {
        ButterKnife.bind(this);
        chapterses=Constant.chaptersList;
        computeText();
        initViewData();
        switchMode(SharedPreUtil.getBoolean(this, Constant.KEY_SHOW_MODE, false));
    }


    @Override
    public void initData() {
      if(chapterses==null){
          pb.setVisibility(View.VISIBLE);
          connect();
      }else {
          pb.setVisibility(View.GONE);
          error.setVisibility(View.GONE);
      }
    }

    //初始化弹窗
    private void initWindows(int position) {
        readView = Global.inflate(R.layout.layout_read_setup, null);
        //默认是没打开设置界面
        readState = false;
        readWindow = new PopupWindow(this);
        readWindow.setHeight(Global.dp2px(150));
        readWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        readWindow.setContentView(readView);
        readWindow.setBackgroundDrawable(new ColorDrawable());
        btn_catalog = readView.findViewById(R.id.btn_catalog);
        btn_mode = readView.findViewById(R.id.btn_mode);
        progressText = readView.findViewById(R.id.progressText);
        progressText.setText("现在阅读的是：" + Constant.BOOK_NAME);
        setTextView = readView.findViewById(R.id.setTextSize);
        initReadWindowTextColor();
        progress = readView.findViewById(R.id.progress);
        if (chapterses != null) {
            progress.setMax(chapterses.size() - 1);
        }
        progress.setProgress(position);
        setTextSizeWindow = new PopupWindow(this);
        setTextSizeWindow.setHeight(Global.dp2px(150));
        setTextSizeWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setTextSizeWindow.setBackgroundDrawable(new ColorDrawable());
        View view = Global.inflate(R.layout.layout_set_textsize);
        setValue = view.findViewById(R.id.value);
        setAdd = view.findViewById(R.id.add);
        setMinus = view.findViewById(R.id.minus);
        setTextSizeWindow.setContentView(view);
        int textSize = SharedPreUtil.getInt(this, Constant.KEY_TEXT_SIZE, 16);
        text.setTextSize(textSize);
        setValue.setText(String.valueOf(textSize));
    }

    //初始化弹窗字体颜色
    private void initReadWindowTextColor() {
        Boolean showMode = getBoolean(this, Constant.KEY_SHOW_MODE, false);
        if (showMode) {
            btn_mode.setText(MODE_DEFAULT);
        } else {
            btn_mode.setText(MODE_NIGHT);
        }
    }

    //初始化View数据
    private void initViewData() {
        Constant.PAINT = text.getPaint();
        viewPagerAdapter = new BookReadParentAdapter();
        viewpager.setAdapter(viewPagerAdapter);
        int position = getIntent().getExtras().getInt(Constant.KEY_POSITION);
        viewpager.setCurrentItem(position, false);
        initWindows(position);
    }


    private void computeText() {
        title.setVisibility(View.VISIBLE);
        text.post(new Runnable() {
            @Override
            public void run() {
                calculationData();
            }
        });
    }

    //计算TextView高度
    private void calculationData() {
        aLineWidth = text.getWidth();
        firstLineNumber = (text.getHeight()) / text.getLineHeight();
        title.setVisibility(View.GONE);
        text.post(new Runnable() {
            @Override
            public void run() {
                otherLineNumber = (text.getHeight()) / text.getLineHeight();
                screen();
            }
        });
    }

    //筛选数据更新
    private void screen() {
        List<BookChapterBean> beanList = viewPagerAdapter.getBeanList();
        if (beanList == null) {
            return;
        }
        if (beanList.size() == 0) {
            return;
        }

        for (BookChapterBean bean : beanList) {
            //在Presenter时没获取到TextView属性 无法进行筛选
            Presenter.getInstance().screenData(bean);
            viewPagerAdapter.updata(viewPagerAdapter.getViews()[bean.getPosition()], bean.getPosition());

        }
    }


    @Override
    public void initListener() {
        setAdd.setOnClickListener(this);
        setMinus.setOnClickListener(this);
        setTextView.setOnClickListener(this);
        btn_catalog.setOnClickListener(this);
        btn_mode.setOnClickListener(this);
        connect.setOnClickListener(this);
        progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int position;

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                position = i;
                if (chapterses == null || chapterses.size() == 0) {
                    progressText.setText("章节：" + chapterses.get(i).getTitle());
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                viewpager.setCurrentItem(position, false);
            }
        });
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private ReadProgress readProgress;
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (readWindow.isShowing()) {
                    progress.setProgress(position);
                }
                //存储上一次读取位置
                if(readProgress==null){
                    readProgress = Presenter.getInstance().selectFirst(ReadProgress.class,"bookName=?", Constant.BOOK_NAME);
                    if(readProgress==null){
                        readProgress=new ReadProgress();
                    }
                }
                readProgress.setPosition(position);
                readProgress.save();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewpager.setOnTouchListener(new View.OnTouchListener() {
            private float downX;
            private float moveX;
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(viewpager.getCurrentItem()==viewPagerAdapter.getCount()-1){
                    if(motionEvent.getAction()==MotionEvent.ACTION_DOWN){
                        downX=motionEvent.getX();
                        LogUtil.d("downx"+downX);
                    }
                    if(motionEvent.getAction()==MotionEvent.ACTION_MOVE){
                        moveX=motionEvent.getX();
                    }
                    if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                        LogUtil.d("downX"+downX+"--moveX"+moveX);
                        if(downX>moveX){
                            Toast.makeText(BookReadActivity.this, "没有更多了", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                return false;
            }
        });
        Constant.readOnclickListener = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (readState) {
                    Global.setFullScreen(getWindow());
                    readWindow.dismiss();
                    readState = false;
                } else {
                    int x = root.getWidth();
                    int y = Global.dp2px(150);
                    LogUtil.d("X:-" + x + "Y:-" + y);
                    readWindow.showAsDropDown(root, x * -1, y * -1);
                    Global.quitFullScreen(getWindow(), BookReadActivity.this);
                    readState = true;
                }
                if (setTextSizeWindow.isShowing()) {
                    setTextSizeWindow.dismiss();
                }
            }
        };
    }


//    private  Toast mToast = null;
//    public void showToast(String text) {
//        Context context=Global.mContext;
//        if (mToast == null) {
//            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//        } else {
//            mToast.setText(text);
//            mToast.setDuration(Toast.LENGTH_SHORT);
//        }
//
//        mToast.show();
//    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.btn_catalog:
                Intent intent = new Intent(this, BookCatalogActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_mode:
                switchText();
                break;
            case R.id.setTextSize:
                setTextSize();
                break;
            case R.id.add:
                addtoMinus(true);
                break;
            case R.id.minus:
                addtoMinus(false);
                break;
            case R.id.connect:
                connect();
                break;
        }
    }

    //未获取到目录的情况下 显示重新连接的TextView监听器
    private void connect() {
        error.setVisibility(View.GONE);
        pb.setProgress(View.VISIBLE);
        Presenter.getInstance().onCatalog(this,Constant.BOOK_ID);
    }

    //字体加减
    private void addtoMinus(boolean isAdd) {
        int value = Integer.parseInt(setValue.getText().toString());
        if (isAdd) {
            value++;
        } else {
            value--;
        }
        setValue.setText(String.valueOf(value));
        SharedPreUtil.saveInt(this, Constant.KEY_TEXT_SIZE, value);
        text.setTextSize(value);
        computeText();
        title.setVisibility(View.VISIBLE);
    }

    //弹出设置大小window
    private void setTextSize() {
        readWindow.dismiss();
        setTextSizeWindow.showAsDropDown(root, (int) Global.mScreenWidth * -1, Global.dp2px(100) * -1);
    }

    //切换夜间白天字体模式
    private void switchText() {
        Boolean showMode = getBoolean(this, Constant.KEY_SHOW_MODE, false);
        if (showMode) {
            showMode = false;
            btn_mode.setText(MODE_NIGHT);
        } else {
            showMode = true;
            btn_mode.setText(MODE_DEFAULT);
        }
        switchMode(showMode);
        SharedPreUtil.saveBoolean(this, Constant.KEY_SHOW_MODE, showMode);
    }

    //日间与夜间模式切换
    private void switchMode(Boolean showMode) {
        Integer color;
        if (showMode) {
            layoutBookRead.setBackgroundColor(getResources().getColor(R.color.black));
            viewPagerAdapter.switchTextColor(Constant.TYPE_COLOR, R.color.white);
            color = R.color.white;
        } else {
            layoutBookRead.setBackgroundColor(getResources().getColor(R.color.Yellowishrice));
            viewPagerAdapter.switchTextColor(Constant.TYPE_COLOR, R.color.black);
            color = R.color.black;
        }
        SharedPreUtil.saveInt(this, Constant.KEY_TEXT_COLOR, color);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (readWindow.isShowing()) {
            readWindow.dismiss();
        }
    }



    @Override
    public void getData(int type, BookRead bookRead) {
        pb.setVisibility(View.GONE);
        for(int i=0;i<bookRead.mixToc.chapters.size();i++){
            BookRead.MixToc.Chapters chapter=bookRead.mixToc.chapters.get(i);
            if(bookRead.mixToc.chapters.size()/2>i){
                chapter.setBookName("bookName："+title);
                chapter.id=type;
            }else {
                chapter.setBookName("bookName：fish最帅");
                chapter.id=type+1;
            }
        }
       Constant.chaptersList=bookRead.mixToc.chapters;
        initView();
        initListener();
        initData();
    }

    @Override
    public void getError(int type, String string) {
        pb.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
    }
}

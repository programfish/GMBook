package ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;

import base.BaseActivity;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.httpbean.CategoryList;
import model.httpbean.ClassificationBean;
import presenter.Presenter;
import ui.adapter.ClassificationAdapter;
import ui.viewInterface.MainView;

/**
 * Created by Administrator on 2017/12/21.
 */

public class ClassificationActivity extends BaseActivity implements MainView<CategoryList, String> {


    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    private ClassificationAdapter adapter;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_classification;
    }

    @Override
    public void initView() {
        Global.setStatusBarColor(this, Global.getColor(R.color.red2));
        ButterKnife.bind(this);
        initRecycler();
    }

    private void initRecycler() {
        recyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new ClassificationAdapter(getApplicationContext(), null);
//        adapter.addDatas();
        recyclerview.setAdapter(adapter);


    }

    @Override
    public void initListener() {
        btnBack.setOnClickListener(this);
    }

    @Override
    public void initData() {
        Presenter.getInstance().onClassification(this);
    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    public void getData(int type, CategoryList data) {
        List<ClassificationBean> beanList = new ArrayList<>();
        ClassificationBean bean1 = new ClassificationBean();
        bean1.setTitle("男生频道");
        bean1.setData(data.male);
        ClassificationBean bean2 = new ClassificationBean();
        bean2.setTitle("女生频道");
        bean2.setData(data.female);
        ClassificationBean bean3 = new ClassificationBean();
        bean3.setTitle("picture");
        bean3.setData(data.picture);
        ClassificationBean bean4 = new ClassificationBean();
        bean4.setTitle("press");
        bean4.setData(data.press);
        beanList.add(bean1);
        beanList.add(bean2);
        beanList.add(bean3);
        beanList.add(bean4);
        adapter.setDatas(beanList);
    }

    @Override
    public void getError(int type, String error) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}

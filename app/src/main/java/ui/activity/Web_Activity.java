package ui.activity;

import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.peng.book.R;

import base.BaseActivity;
import base.Constant;
import butterknife.BindView;
import butterknife.ButterKnife;
import util.LogUtil;

public class Web_Activity extends BaseActivity {

    public static final String TAG = "Main_Activity";
    @BindView(R.id.webSplash)
    WebView webSplash;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Override
    public int getLayoutRes() {
        return R.layout.web_activity;
    }

    @Override
    public void initView() {


        ButterKnife.bind(this);
        initWebSplash();
    }

    private void initWebSplash() {
        String linkUrl = getIntent().getStringExtra(Constant.LINK_URL_FLAG);
        if (!TextUtils.isEmpty(linkUrl)) {
            WebSettings webSettings = webSplash.getSettings();
            webSettings.setJavaScriptEnabled(true);

            webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            //关闭webview中缓存
            webSettings.setAllowFileAccess(true);
            //设置可以访问文件
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            //支持通过JS打开新窗口
            webSettings.setLoadsImagesAutomatically(true);
            //支持自动加载图片
            webSettings.setDefaultTextEncodingName("utf-8");
            //设置编码格式


            webSplash.loadUrl(linkUrl);

            webSplash.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            LogUtil.d(TAG, linkUrl);
            webSplash.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    @Override
    public void initListener() {
        ivBack.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        if(id == R.id.ivBack){
            finish();
        }
    }


}

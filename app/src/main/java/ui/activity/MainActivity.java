package ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import ui.adapter.MainTabAdapter;
import ui.fragment.BookRankingListFragment;
import ui.fragment.BookRecommendFragment;
import ui.fragment.BookShelfFragment;
import util.LogUtil;

public class MainActivity extends BaseActivity {


    @BindView(R.id.recycler)
    RecyclerView recycler;

    public static final String TAG = "Main_Activity";

    private static final int LAYOUT_ID=R.id.frame;
    private List<Fragment> fragmentList;
    private Integer fristNumber=0;
    private List<String> stringList;
    private BookShelfFragment bookShelfFragment;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        Global.setStatusBarColor(MainActivity.this, Global.getColor(R.color.red2));
        ButterKnife.bind(this);
        initFragment();
        initRecycler();
    }

    public BookShelfFragment getBookShelfFragment() {
        return bookShelfFragment;
    }

    private void initFragment(){
        stringList=new ArrayList<>();
        stringList.add("BookShelfFragment");
        stringList.add("BookRecommendFragment");
        stringList.add("BookRankingListFragment");
        fragmentList = new ArrayList<>();
        bookShelfFragment = new BookShelfFragment();
        fragmentList.add(bookShelfFragment);
        BookRecommendFragment bookRecommendFragment = new BookRecommendFragment();
        fragmentList.add(bookRecommendFragment);
        BookRankingListFragment bookRankingListFragment = new BookRankingListFragment();
        fragmentList.add(bookRankingListFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .add(LAYOUT_ID, bookShelfFragment,stringList.get(0))
                .show(bookShelfFragment)
                .commit();

    }

    public void switchFragment(int position){
        FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
        if(!fragmentList.get(position).isAdded()){
            trx.add(LAYOUT_ID,fragmentList.get(position),stringList.get(position));
        }
        trx.show(fragmentList.get(position));
        if(fristNumber!=position){
            trx.hide(fragmentList.get(fristNumber));
            LogUtil.d("隐藏Fragment"+fragmentList.get(fristNumber).getTag());
        }
        trx.commit();
        fristNumber=position;
    }

    private void initRecycler() {
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recycler.setLayoutManager(linearLayoutManager);
        List<HashMap<String,Object>> hashMapList=new ArrayList<>();
        HashMap<String,Object> hashMap;
        hashMap=new HashMap<>();
        hashMap.put(Constant.HASHMAP_KEY_TEXT,"书架");
        hashMap.put(Constant.HASHMAP_KEY_IMAGE,R.drawable.tab_bookshelf);
        hashMapList.add(hashMap);
        hashMap=new HashMap<>();
        hashMap.put(Constant.HASHMAP_KEY_TEXT,"精选");
        hashMap.put(Constant.HASHMAP_KEY_IMAGE,R.drawable.tab_exquisite);
        hashMapList.add(hashMap);
        hashMap=new HashMap<>();
        hashMap.put(Constant.HASHMAP_KEY_TEXT,"排行榜");
        hashMap.put(Constant.HASHMAP_KEY_IMAGE,R.drawable.tab_list);
        hashMapList.add(hashMap);
        MainTabAdapter adapter=new MainTabAdapter(MainActivity.this,hashMapList);
        recycler.setAdapter(adapter);
    }


    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        isStartWeb();
    }

    @Override
    public void onClick(View v, int id) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public void isStartWeb() {
        if(!getIntent().getBooleanExtra(Constant.SPLASH_START_WEB_FLAG, false)){
            return;
        }
        LogUtil.d(TAG, "用户已点击广告页");
        String linkUrl = getIntent().getStringExtra(Constant.LINK_URL_FLAG);
        if(!TextUtils.isEmpty(linkUrl)){
            LogUtil.d(TAG, "广告页地址可访问正在执行跳转--WEB");
            Bundle bundle = new Bundle();
            bundle.putString(Constant.LINK_URL_FLAG, linkUrl);
            Global.startActivity(Web_Activity.class, bundle);
        }
    }
}

package ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.util.Timer;
import java.util.TimerTask;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import presenter.SplashPresenter;
import ui.viewInterface.SplashInterface;
import util.LogUtil;

public class Splash_Activity extends BaseActivity implements SplashInterface {

    private static final String TAG = "Splash_Activity";

    private static boolean FLAG = false; //是否跳转Main

    private Bitmap bitmap;

    private String linkUrl;

    private static boolean START_WEB_FLAG = false; //用户是否点击广告页

    private static boolean START_COUNTDOWN = false; // 是否倒计时完成过一次

    @BindView(R.id.ivPicture)
    ImageView ivPicture;
    @BindView(R.id.btnSkip)
    Button btnSkip;

    private SplashPresenter presenter;

    private Timer timer;

    @Override
    public int getLayoutRes() {
        Global.setFullScreen(getWindow());
        return R.layout.splash_activity;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);

        timer = new Timer();

        countDown(Constant.SPLASH_WAIT_TIME);

        presenter = new SplashPresenter(this);
        presenter.request(System.currentTimeMillis());

    }

    @Override
    public void initListener() {
        btnSkip.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v, int id) {
        if (id == R.id.btnSkip) {
            LogUtil.d(TAG, "用户点击跳转按钮 执行下一步");
            startMain();
        }
    }

    /**
     * 跳转至Main_Activity 携带两个参数
     * 一个是广告地址，一个是用户是否点击广告页的布尔变量
     */
    public void startMain() {
        if (FLAG) {
            return;
        }

        LogUtil.d(TAG, "执行跳转");

        Bundle bundle = new Bundle();
        bundle.putString(Constant.LINK_URL_FLAG, linkUrl);
        bundle.putBoolean(Constant.SPLASH_START_WEB_FLAG, START_WEB_FLAG);

        bitmap = null;
        linkUrl = null;

        Global.startActivity(MainActivity.class, bundle);
        FLAG = true;
        finish();
    }

    /**
     * 如果广告链接为空，点击ImageView不会直接进入Main
     *
     * @param bitmap
     * @param linkUrl 广告链接
     */
    @Override
    public void showImage(final Bitmap bitmap, final String linkUrl) {

        this.bitmap = bitmap;
        this.linkUrl = linkUrl;


        ivPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(linkUrl)) {
                    return;
                }
                if(bitmap == null){
                    return;
                }
                START_WEB_FLAG = true;
                startMain();
            }
        });
    }

    @Override
    public void requestFail() {

    }



    /**
     * 定时器
     */
    public void countDown(int time) {
        timer.schedule(new CountTime(), time);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
        timer.cancel();
    }

    class CountTime extends TimerTask{

        @Override
        public void run() {


            Observable.just("")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<String>() {
                        @Override
                        public void accept(@NonNull String s) throws Exception {
                            LogUtil.d(TAG, "定时器等待完毕 开始执行任务");
                            if (START_COUNTDOWN) {
                                startMain();
                            } else {
                                START_COUNTDOWN = true;
                                if (bitmap != null){
                                    ivPicture.setImageBitmap(bitmap);
                                    btnSkip.setVisibility(View.VISIBLE);
                                    countDown(Constant.SPLASH_WAIT_TIME * 2);
                                }else {
                                    startMain();
                                }
                            }
                        }
                    });


        }
    }


}

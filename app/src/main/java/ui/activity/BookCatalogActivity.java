package ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.peng.book.R;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import base.BaseActivity;
import base.Constant;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import model.dbbean.ReadProgress;
import model.httpbean.BookRead;
import presenter.Presenter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import ui.adapter.CatalogAdapter;
import ui.viewInterface.MainView;
import util.LogUtil;

import static android.R.attr.type;


public class BookCatalogActivity extends BaseActivity implements MainView<BookRead, String> {

    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.direction)
    TextView direction;
    @BindView(R.id.sequence)
    LinearLayout sequence;
    @BindView(R.id.activity_book_catalog)
    LinearLayout activityBookCatalog;
    @BindView(R.id.list)
    StickyListHeadersListView list;
    @BindView(R.id.chapterCount)
    TextView chapterCount;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.error)
    LinearLayout error;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;
    private String id;
    private CatalogAdapter adapter;
    private Intent intent;
    private String title;
    private ReadProgress readProgress;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_book_catalog;
    }

    @Override
    public void initView() {
        Global.setStatusBarColor(this, Global.getColor(R.color.red2));
        ButterKnife.bind(this);
        initIntent();
        initListView();
    }

    private void initToPosition() {
        readProgress = Presenter.getInstance().selectFirst(ReadProgress.class, "bookName=?", Constant.BOOK_NAME);
        if (readProgress == null) {
            readProgress = new ReadProgress();
            LogUtil.d("没有进度数据");
        }
    }

    private void initListView() {
        adapter = new CatalogAdapter(this, null);
        list.setAdapter(adapter);
    }

    private void initIntent() {
        intent = new Intent(BookCatalogActivity.this, BookReadActivity.class);
        id = Constant.BOOK_ID;
        title = Constant.BOOK_NAME;
    }


    private void initListViewOnItemClick() {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                intent.putExtra(Constant.KEY_POSITION, i);
                Constant.chaptersList = adapter.getListData();
                startActivity(intent);
                readProgress.setBookName(Constant.BOOK_NAME);
                readProgress.setBookId(Constant.BOOK_ID);
                readProgress.setPosition(i);
                Presenter.getInstance().saveData(readProgress, "进度存储");
                finish();
            }
        });
    }

    @Override
    public void initData() {
        if(Constant.chaptersList!=null){
            setData(Constant.chaptersList);
        }else {
            refresh.setRefreshing(true);
            Presenter.getInstance().onCatalog(this, id);
        }

        initToPosition();
    }

    @Override
    public void onClick(View v, int id) {
        switch (id) {
            case R.id.sequence:
                sequenceClick();
                break;
        }
    }

    @Override
    public void initListener() {
        initListViewOnItemClick();
        btnBack.setOnClickListener(this);
        sequence.setOnClickListener(this);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(error.getVisibility()==View.VISIBLE){
                    error.setVisibility(View.GONE);
                }
                Presenter.getInstance().onCatalog(BookCatalogActivity.this, id);
            }
        });
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if(i==0){
                    refresh.setEnabled(true);
                }else {
                    refresh.setEnabled(false);
                }
            }
        });
    }


    private boolean isSequence = false;

    //true 正序 false倒序
    private void sequenceClick() {
        List<BookRead.MixToc.Chapters> list = adapter.getListData();

        if (isSequence) {
            isSequence = false;
            Collections.reverse(list);
            adapter.setDatas(list);
            direction.setText("正序");
        } else {
            direction.setText("倒序");
            isSequence = true;
            Collections.reverse(list);
            adapter.setDatas(list);
        }
    }


    @Override
    public void getData(int type, BookRead bookRead) {
        setData(bookRead.mixToc.chapters);
    }

    private void setData(List<BookRead.MixToc.Chapters> chaptersList) {
        for (int i = 0; i < chaptersList.size(); i++) {
            BookRead.MixToc.Chapters chapter = chaptersList.get(i);
            if (chaptersList.size() / 2 > i) {
                chapter.setBookName("bookName：" + title);
                chapter.id = type;
            } else {
                chapter.setBookName("bookName：fish最帅");
                chapter.id = type + 1;
            }
        }
        adapter.setDatas(chaptersList);
        chapterCount.setText("共" + chaptersList.size() + "章");
        list.post(new Runnable() {
            @Override
            public void run() {
                if (readProgress.getPosition() == null) {
                    return;
                }
                list.setSelection(readProgress.getPosition());
            }
        });
        refresh.setRefreshing(false);
        content.setVisibility(View.VISIBLE);
    }

    public void delayRefresh(){
        Flowable.timer(1, TimeUnit.SECONDS, Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long aLong) throws Exception {
                        refresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public void getError(int type, String error) {
        delayRefresh();
        if(content.getVisibility()==View.VISIBLE){
            this.error.setVisibility(View.GONE);
        }else {
            this.error.setVisibility(View.VISIBLE);
        }
        Toast.makeText(this, "网络请求失败，下拉刷新更多", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}

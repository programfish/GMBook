package ui.activity;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.peng.book.R;

import java.util.ArrayList;
import java.util.List;

import base.BaseActivity;
import base.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.dbbean.SearchRecord;
import model.httpbean.AutoComplete;
import presenter.Presenter;
import ui.fragment.FragmentAutoComplete;
import ui.fragment.FragmentHotWords;
import ui.fragment.FragmentSearchRecord;
import ui.fragment.FragmentSearchResult;
import ui.viewInterface.MainView;
import util.LogUtil;

/**
 * Created by Administrator on 2017/12/22.
 */

public class SearchActivity extends BaseActivity implements MainView<AutoComplete,String>{


    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;


    @BindView(R.id.edit)
    EditText edit;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.line)
    View line;

    private static final int FRAME_ID = R.id.framelayout;
    private List<SearchRecord> searchRecordList;
    private int index;
    private List<Fragment> fragmentList;
    public static final int HOTWORDS=0;
    public static final int SEARCHRECORD=1;
    public static final int AUTOCOMPLETE=2;
    public static final int SEARCHRESULT=3;
    private FragmentManager manager;


    @Override
    public int getLayoutRes() {
        return R.layout.activity_search;
    }

    @Override
    public void initView() {
        Global.setStatusBarColor(this, Global.getColor(R.color.red2));
        ButterKnife.bind(this);
        initFragment();
        initEdit();
    }

    public void initEdit() {
        isFocus=false;
        edit.setFocusable(false);
        edit.setFocusableInTouchMode(false);
    }

    private void initFragment() {
        FragmentHotWords hotWords=new FragmentHotWords();
        FragmentSearchRecord searchRecord=new FragmentSearchRecord();
        FragmentAutoComplete autoComplete=new FragmentAutoComplete();
        FragmentSearchResult searchResult=new FragmentSearchResult();
        index=HOTWORDS;
        fragmentList = new ArrayList<>();
        fragmentList.add(hotWords);
        fragmentList.add(searchRecord);
        fragmentList.add(autoComplete);
        fragmentList.add(searchResult);
        manager = getSupportFragmentManager();
        manager.beginTransaction()
                .add(FRAME_ID,hotWords)
                .add(FRAME_ID,searchRecord)
                .add(FRAME_ID,autoComplete)
                .add(FRAME_ID,searchResult)
                .show(hotWords)
                .hide(searchRecord)
                .hide(autoComplete)
                .hide(searchResult)
                .commit();
    }



    public void requestSearchBooks(String content){
        manager.beginTransaction()
                .hide(fragmentList.get(HOTWORDS))
                .hide(fragmentList.get(SEARCHRECORD))
                .hide(fragmentList.get(AUTOCOMPLETE))
                .show(fragmentList.get(SEARCHRESULT))
                .commit();
        FragmentSearchResult result= (FragmentSearchResult) fragmentList.get(SEARCHRESULT);
        result.requestData(content);
        index=SEARCHRESULT;
    }


    private boolean isFocus=false;
    @Override
    public void initListener() {
        btnBack.setOnClickListener(this);
        search.setOnClickListener(this);
        edit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                    if(!isFocus){
                        edit.setFocusableInTouchMode(true);
                        edit.setFocusable(true);
                        isFocus=true;
                        if(index==HOTWORDS){
                            selectDB();
                            if(searchRecordList!=null&&searchRecordList.size()!=0){
                                LogUtil.d("OnTouchListener--HOTWORDS");
                                switchFragment(fragmentList.get(SEARCHRECORD),fragmentList.get(HOTWORDS));
                                switchSEARCHRECORD();
                                index=SEARCHRECORD;
                            }
                        }
                    }
                }

                return false;
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switchFragment(editable.toString().toString().trim());
            }

        });

        edit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_ENTER){
                    search(edit.getText().toString().trim());
                    Global.keyboard(true,SearchActivity.this);
                    return true;
                }
                return false;
            }
        });

    }

    /**
     *  当搜索框发生改变后根据当前index进行切换Fragment
     * @param content editText内容
     */
    private void switchFragment(String content) {
        switch (index){
            case HOTWORDS:
                if(!TextUtils.isEmpty(content)||content.length()!=0){
                    switchAUTOCOMPLETE(content);
                }
                break;
            case SEARCHRECORD:
                if(!TextUtils.isEmpty(content)||content.length()!=0){
                   switchAUTOCOMPLETE(content);
                }
                break;
            case AUTOCOMPLETE:
                if(TextUtils.isEmpty(content)||content.length()==0){
                    selectDB();
                    if(searchRecordList!=null&&searchRecordList.size()!=0){
                        switchFragment(fragmentList.get(SEARCHRECORD),fragmentList.get(AUTOCOMPLETE));
                        switchSEARCHRECORD();
                        index=SEARCHRECORD;
                    }else {
                        switchFragment(fragmentList.get(HOTWORDS),fragmentList.get(AUTOCOMPLETE));
                        index=HOTWORDS;
                    }
                }else {
                    Presenter.getInstance().onAutoComplete(this,content);
                }
                break;
            case SEARCHRESULT:
                if(!TextUtils.isEmpty(content)||content.length()!=0){
                    Presenter.getInstance().onAutoComplete(this,content);
                    switchFragment(fragmentList.get(AUTOCOMPLETE),fragmentList.get(SEARCHRESULT));
                    index=AUTOCOMPLETE;
                }else {
                    switchFragment(fragmentList.get(HOTWORDS),fragmentList.get(AUTOCOMPLETE));
                    index=HOTWORDS;
                }
                break;
        }
        LogUtil.d("switchFragment--"+index);
    }


    /**
     *  切换至FragmentAutoComplete
     * @param content 搜索内容
     */
    private void switchAUTOCOMPLETE(String content){
        Presenter.getInstance().onAutoComplete(this,content);
        switchFragment(fragmentList.get(AUTOCOMPLETE),fragmentList.get(index));
        index=AUTOCOMPLETE;
    }


    /**
     *  切换至FragmentsearchRecord
     *  更新Recycler数据
     */
    private void switchSEARCHRECORD(){
        FragmentSearchRecord record= (FragmentSearchRecord) fragmentList.get(SEARCHRECORD);
        record.initSearchData(searchRecordList);
    }


    /**
     * 以下两个方法：使Edit点击空白地方时关闭软键盘失去焦点
     * 来自百度-简书
     */
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                //使EditText触发一次失去焦点事件
                v.setFocusable(false);
//                v.setFocusable(true); //这里不需要是因为下面一句代码会同时实现这个功能
                v.setFocusableInTouchMode(true);
                return true;
            }
        }
        return false;
    }




    @Override
    public void initData() {

    }


    /**
     * 查询数据库历史记录
     */
    private void selectDB(){
        searchRecordList =
                Presenter.getInstance().selectListOrder("searchTime desc", SearchRecord.class);
    }

    @Override
    public void onClick(View v, int id) {
        switch (id){
            case R.id.search:
                String searchContent=edit.getText().toString().trim();
                if(TextUtils.isEmpty(searchContent)||searchContent.length()==0){
                    return;
                }
                search(searchContent);
                break;
        }
    }


    /**
     *  清除edit内容
     */
    public void clearEditText(){
        edit.setText(null);
    }


    /**
     *  1、查询本地数据库是否有该记录
     *     有则更新关键词时间，无则保存
     *  2、edit失去焦点
     *  3、查询书籍，切换到查询书籍的页面
     */
    public void search(String searchContent) {

        SearchRecord bean= Presenter.getInstance()
                .selectFirst(SearchRecord.class,"recordContent=?",searchContent);
        if(bean!=null){
            bean.setSearchTime(System.currentTimeMillis());
            Presenter.getInstance()
                    .updata(bean,"recordContent=?",bean.getRecord());
        }else {
            bean=new SearchRecord();
            bean.setSearchTime(System.currentTimeMillis());
            bean.setRecord(searchContent);
            Presenter.getInstance()
                    .saveData(bean,"搜索记录");
        }
        edit.setText(searchContent);
        initEdit();
        requestSearchBooks(searchContent);

    }


    /**
     *  用于FragmentSearchRecord清楚历史记录
     *  切换Fragment
     */
    public void SearchRecordToHotWrods(){
        switchFragment(fragmentList.get(HOTWORDS),fragmentList.get(SEARCHRECORD));
        index=HOTWORDS;
    }

    @Override
    public void getData(int type, AutoComplete data) {
        FragmentAutoComplete autoComplete= (FragmentAutoComplete) fragmentList.get(AUTOCOMPLETE);
        autoComplete.setRecyclerData(data.keywords);
        LogUtil.d("关键字补全获取数据--"+data.keywords.size());
    }

    @Override
    public void getError(int type, String error) {
        LogUtil.d(error);
    }


    /**
     *  监听返回键且消费事件
     *  以此达到回退栈效果
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            LogUtil.d("onKeyDown--"+index);
            switch (index){
                case AUTOCOMPLETE:
                    selectDB();
                    if(searchRecordList.size()!=0&&searchRecordList!=null){
                        switchFragment(fragmentList.get(SEARCHRECORD),fragmentList.get(AUTOCOMPLETE));
                        switchSEARCHRECORD();
                        index=SEARCHRECORD;
                    }
                    break;
                case SEARCHRECORD:
                    switchFragment(fragmentList.get(HOTWORDS),fragmentList.get(SEARCHRECORD));
                    index=HOTWORDS;
                    break;
                case HOTWORDS:
                    finish();
                    break;
                case SEARCHRESULT:
                    manager.beginTransaction()
                            .hide(fragmentList.get(SEARCHRESULT))
                            .hide(fragmentList.get(SEARCHRECORD))
                            .hide(fragmentList.get(AUTOCOMPLETE))
                            .show(fragmentList.get(HOTWORDS))
                            .commit();
                    index=HOTWORDS;
                    break;

            }
            clearEditText();
            initEdit();
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }


}
